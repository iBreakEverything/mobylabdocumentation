# Website

Aceasta este documentia MobyLab pentru a sustine materialele de laborator pentru diverse materii si workshop-uri ale Facultatii de Automatica si Calculatoare, UNSTPB pe site-ul [mobylab.docs.crescdi.pub.ro](https://mobylab.docs.crescdi.pub.ro/).


## Instalare

Pentru a porni proiectul trebuie instalata ultima versiune de [NodeJs](https://nodejs.org/en).

Dupa instalare se descarca codul de pe gitlab cu:

```
git clone https://gitlab.com/mobylabwebprogramming/mobylabdocumentation
```

Si se deschide cu VisualStudio Code proiectul unde trebuie sa se instaleze dependentele de in package.json cu:

```
npm install
```

## Pornire

Pentru a porni proiectul rulati:

```
npm run start
```

Va porni automat si se va deschide o fereastra noua de browser pe http://localhost:3000/.

Pentru a rula in limba engleza se poate rula comanda:

```
npm run start -- --locale en
```

## Modificare

Pentru a modifica continutul postat pe site doar trebuie adaugat/modificat un fisier in format Markdown care reprezinta pagina cu continut cu extensia .md sau mdx.

Daca doriti de exemplu sa adaugati pentru materia Algoritmi Paraleli și Distribuiți trebuie doar sa adaugati fiierele markdown in folderul corespunzator care in acest caz este "docs/parallelAndDistributed" cu urmatorul antet:

```
---
title: <titlu care sa se vada in sidebar>
sidebar_position: <pozitia paginii in sidebar>
---
````

Paginile se for adauga automat in sidebar cand sunt adaugate in folder, daca doriti sa aveti intrarile in sidebar imbricate creati folder nou si pagina care sa contina celelalte intrari trebuie sa fie numit "index.mdx" unde mai adaugati cateva informatii de prezentare ca la orice alta pagina cu antetul mentionat si la final cu urmatoarea bucata de cod asa cum este la alte pagini index.mds

```
import DocCardList from '@theme/DocCardList';

<DocCardList />
```

Pentru a adauga imgini le puneti in "static/img" si le introduceti in Markdown stilizat ca HTML:

```
<img alt="C#" src="/img/dotnet/csharp.webp" width="200" style={{float: "right"}} />
```

Dupa ce ati modificat puneti pe un branch separat modificarile si faceti un mergi request catre o persoana responsabila din catrul echipei pentru laborator.

## Traduceri

Daca vreti sa puneti si traduceri copiati fisierele din "docs" in "18n/en/docusaurus-plugin-content-docs/current/&lt;folder corespunzator&gt;" si adaugati traducerea acolo (hit: ChatGPT).

## Afisare avansata

Daca doriti sa adagati formule matelatice ca in Latex aveti posibilitatea prin plugin-ul de Katex, doar trebuie adaugate in fisierele Markdown formulele de Latex. Mai multe informatii sunt [aici](https://docusaurus.io/docs/markdown-features/math-equations).

Pe langa formule se pot adauga si diagrame direct prin [Mermaid](http://mermaid.js.org/intro/) in Markdown ca text in loc sa fie adaugate ca poze din alta aplicatie. Mai multe informatii de cum se pot adauga diagrame aveti [aici](https://docusaurus.io/docs/markdown-features/diagrams).

## P.S.

Anumite lucruri cum e escaparea de &lt;&gt; sau exemple de cod Markdown exista deja in cod si puteti doar sa va uitati acolo. Nu utiati ca fisierele Markdown sunt traduse in componente de React, daca folositi HTML stilul trebuie definit ca obiect cu "{{}}" nu ca CSS inline.
