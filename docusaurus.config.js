// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

import math from 'remark-math';
import katex from 'rehype-katex';

import { themes } from 'prism-react-renderer';

const lightTheme = themes.github;
const darkTheme = themes.dracula;

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'MobyLab',
  tagline: 'In constant pursuit of more knowledge',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://mobylab.docs.crescdi.pub.ro',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  //organizationName: 'facebook', // Usually your GitHub org/user name.
  //projectName: 'docusaurus', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    path: 'i18n',
    defaultLocale: 'ro',
    locales: ['ro', 'en'],
    localeConfigs: {
      ro: {
        htmlLang: 'ro-RO',
        path: "ro"
      },
      en: {
        htmlLang: 'en-US',
        path: "en"
      }
    }
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl: 'https://gitlab.com/mobylabwebprogramming/mobylabdocumentation/-/tree/main',
          remarkPlugins: [math],
          rehypePlugins: [katex]
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl: 'https://gitlab.com/mobylabwebprogramming/mobylabdocumentation/-/tree/main',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  stylesheets: [{
    href: '/katex/katex.min.css',
    type: 'text/css',
  }, ],

  themeConfig:
  /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
    // Replace with your project's social card
    image: 'img/docusaurus-social-card.jpg',
    navbar: {
      title: 'Documentatia MobyLab',
      logo: {
        alt: 'Mobylab Logo',
        src: 'img/moby.svg',
      },
      items: [{
          type: 'dropdown',
          label: 'parallelAndDistributedSystems',
          position: 'left',
          items: [{
              type: 'docSidebar',
              sidebarId: 'parallelAndDistributedSidebar',
              label: 'parallelAndDistributedAlgorithms',
            },
            {
              type: 'docSidebar',
              sidebarId: 'webProgrammingSidebar',
              label: 'webProgramming',
            },
            {
              type: 'docSidebar',
              sidebarId: 'softwareDevelopmentSidebar',
              label: 'softwareDevelopment',
            },
            {
              type: 'docSidebar',
              sidebarId: 'cloudComputingSidebar',
              label: 'cloudComputing',
            },
          ],
        },
        {
          type: 'dropdown',
          label: 'programmingLanguages',
          position: 'left',
          items: [{
            type: 'docSidebar',
            sidebarId: 'dotnetSidebar',
            label: 'fundamental',
          }],
        },
        {
          to: 'blog', 
          label: 'blog', 
          position: 'left'
        },
        {
          type: 'localeDropdown',
          position: 'right'
        },
        {
          href: 'https://gitlab.com/mobylabwebprogramming/mobylabdocumentation',
          label: 'gitLab',
          position: 'right',
        }
      ],
    },
    footer: {
      style: 'dark',
      links: [{
          title: 'community',
          items: [{
            label: 'ourLaboratory',
            href: 'https://mobylabupb.com/',
          }],
        },
        {
          title: 'more',
          items: [{
            label: 'gitLab',
            href: 'https://gitlab.com/mobylabwebprogramming/mobylabdocumentation',
          }, ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} MobyLab Built with Docusaurus.`,
    },
    prism: {
      theme: lightTheme,
      darkTheme: darkTheme,
      additionalLanguages: ["csharp", "c", "cpp", "java", "kotlin", "yaml", "javascript", "typescript", "json", "haskell", "scala", "python", "jsx", "tsx", "css", "scss", "sass"]
    },
    docs: {
      sidebar: {
        hideable: true,
      },
    },
  }),
  markdown: {
    mermaid: true,
  },
  themes: [
    [
      require.resolve("@easyops-cn/docusaurus-search-local"),
      ({
        hashed: true,
        language: ["ro", "en"],
      })
    ],
    '@docusaurus/theme-mermaid'
  ]
};

module.exports = config;
module.exports['scripts'] = [{ 'src': '/scripts/arrow-navigate.js', 'async': false }]