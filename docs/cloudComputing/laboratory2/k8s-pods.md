---
title: Crearea și rularea unui pod Kubernetes
sidebar_position: 6
---

Putem crea și rula pod-uri în două maniere: imperativă (prin comenzi cu parametri) și declarativă (folosind fișiere de configurare).

Dacă dorim să rulăm un pod în mod declarativ, folosim următoarea comandă: `kubectl run <pod-name> –image <image name>`. Exemple practice de folosire:
``` shell
kubectl run my-nginx --image=nginx
kubectl run alpine --image=alpine
```

Dacă dorim ca un pod să ruleze interactiv (mai sus aveți exemple de pod-uri care rulează în background), folosim comanda `kubectl run -i –tty –rm <pod-name> –image=<image name>`. Exemplu de folosire: `kubectl run -i –tty –rm alpine –image=alpine`

Un pod poate rula în cadrul unui namespace. Dacă dorim acest lucru, creăm mai întâi un namespace folosind comanda `kubectl create namespace <namespace-name>`. Pentru a rula un pod în cadrul unui namespace: `kubectl run alpine –image=alpine -n <namespace-name>`

Dacă dorim să afișăm toate pod-urile folosim comanda `kubectl get pods` și dacă dorim să listăm toate namespace-urile folosim `kubectl get namespaces`. De asemenea, având în vedere că un pod poate rula în cadrul unui namespace, putem să afișăm toate pod-urile din cadrul unui namespace: `kubectl get pods -n <namespace>`

Pentru a obține mai multe informații cu ajutorul comenzii get, putem să folosim următoarea sintaxă: `kubectl get pods -o wide`

Pentru a rula o comanda in interiorul unui pod, folosim subcomanda exec:
```shell
kubectl exec -it <podname> <command>
kubectl exec -it mypod ls # (mypod trebuie sa existe deja in cluster)
```

:::note Task
Creați un namespace cu numele `my-first-namespace`, rulați două pods (unul cu imaginea alpine, altul cu imaginea nginx) în cadrul namespace-ului creat, afișati namespace-urile și pod-urile din cadrul acelui namespace. Rulați comanda ls în ambele pod-uri.
:::

După cum s-a putut observa mai sus, pentru crearea unui cluster am folosit un fișier de configurare de tip YAML (aici am folosit metoda declarativă). Orice fișier YAML are 4 [componente](https://kubernetes.io/docs/concepts/overview/working-with-objects/kubernetes-objects/) importante:
- **apiVersion** - versiunea de comenzi folosită (asemănător cu version din Docker Compose / Docker Swarm)
- **kind** - tipul de obiect (e.g.: Pod, Deployment, etc.)
:::caution
apiVersion diferă în funcție de kind (exemplu: în cazul deployment-ului este apps/v1, în cazul pod-ului este v1)
:::
- **metadata** - informații adiționale atașate unui obiect (e.g.: name, labels)
- **spec** - conținutul obiectului (asemănător cu ce se scrie în directiva `service` din Docker Compose / Docker Swarm)

Pentru a face deploy la unul (sau mai multe) obiecte dintr-un fișier YAML folosim următoarea comandă: `kubectl apply -f myfile.yaml`

:::note
Obiectul va avea tipul definit în YAML (de exemplu: cluster, pod, deployment, etc.)
:::

:::tip
Se pot folosi și comenzile `kubectl create`, `kubectl update`, `kubectl delete`, dar este indicat să folosiți direct `kubectl apply`, care combină create, replace și delete. Aceasta reprezintă cel mai avansat model de aplicare a unei configurații declarative.
:::

:::note
Obiectul va avea tipul definit în YAML (de exemplu: cluster, pod, deployment, etc.)
:::

Putem crea pod-uri și folosind metoda declarativă, mai precis prin fișiere de configurare. Creăm un fișier de configurare, cu numele `nginxpod.yaml`:
```yaml
apiVersion: v1
kind: Pod
metadata: 
  name: nginx01 # numele pod-ului
spec: 
  containers: 
    - image: nginx
      name: nginx
```

Pentru a crea acest pod, folosim comanda `apply` și specificăm fișierul: `kubectl apply -f nginxpod.yaml`. Se obține un pod cu numele `nginx01`.

:::note Task
Creați un pod în mod declarativ folosind acest [fișier de configurare](https://gitlab.com/mobylab-cc/laborator-4/-/blob/main/testapp-pod.yaml).
:::

De asemenea, în lucrul cu pods putem face următoarele lucruri:
```shell
kubectl port-forward <nume-pod> 8888:5000           # realizeaza mapare de porturi
kubectl logs <nume-pod>                             # afiseaza loguri
kubectl exec <nume-pod> -- ls                       # executa o comanda intr-un pod
kubectl cp file.txt <nume-pod>:/file.txt            # copiaza un fisier intr-un pod
kubectl delete pods/<nume-pod>                      # sterge un pod
```

:::note Task
Aplicați comenzile menționate mai sus pe pod-ul creat în task-ul anterior.
:::