---
title: Labels și Selectors
sidebar_position: 8
---

În fișierul YAML generat de comanda anterioară, putem observa că în câmpul `metadata`, pe lângă atributul name, avem și atributul `labels`:
```yaml
metadata:
  creationTimestamp: null
  labels:
    run: nginx
  name: nginx
```

Aceste labels sunt perechi de tipul cheie-valoare care sunt folosite pentru identificarea obiectelor din Kubernetes. În exemplul de mai sus, avem perechea `run=nginx`. Această pereche poate fi folosită de către un label selector pentru a referi acest obiect (mai multe detalii în continuare).

## Label Selectors
Spre deosebire de nume, label-urile nu asigură unicitate. În general, ne așteptăm ca mai multe obiecte să aibă aceleași label-uri.

Asa cum am spus anterior, cu ajutorul unor Label Selectors putem identifica numite seturi de obiecte în Kubernetes. Un exemplu relevant este expunerea unui pod printr-un serviciu.

Un exemplu comun de utilizare a labels și labels selectors prin intermediul fișierelor de configurare YAML este următorul: `kubectl run nginx –image=nginx –port=8080 –dry-run=client -o yaml >vtest.yaml`

Adăugați pod-ului următorul label:
```yaml
app: myapp
```

Creați pod-ul:
```shell
kubectl apply -f test.yaml.
kubectl describe pod nginx # analizate label-urile podului.
```

Pentru a expune acest pod trebuie creat un serviciu de tipul ClusterIP care să selecteze podul nostru prin intermediul label selectors. Un fișier de configurare YAML pentru acest serviciu este următorul:
```yaml
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null    
  name: nginx
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: myapp #select the pod/pods
status:
  loadBalancer: {}
```

Observați că nu am definit tipul de serviciu, astfel Kubernetes a creat by default un serviciu de tipul ClusterIP. Verificați faptul că podul nginx este expus.