---
title: Crearea și rularea unui cluster Kubernetes
sidebar_position: 5
---

Pentru pornirea unui cluster de Kubernetes pe local se poate folosi următoarea comandă: `minikube start``.

O altă modalitate de a crea un cluster Kubernetes este folosind `kind`. Instrucțiunile de setup pentru acest utilitar le aveți [aici](https://kind.sigs.k8s.io/). `kind` rulează Kubernetes în Docker, simulând câte un nod din cluster cu câte un container.

Pentru crearea unui cluster folosind `kind` ne putem folosi de un fișier de configurare:
```yaml
# Configurare de cluster cu trei noduri (dintre care doi workeri)
kind: Cluster # precizăm ce dorim să creăm: cluster, pod, etc.
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
- role: worker
- role: worker
```
Pentru a crea un cluster folosind `kind` și un fișier de configurare, folosim următoarea comandă: `kind create cluster –config kind-config.yaml`, unde `kind-config.yaml` reprezintă numele fișierului de configurare a cluster-ului.

Dacă dorim să aflăm informații despre cluster-ul curent folosim următoarea comandă: `kubectl cluster-info [dump]`

Pentru a afișa date despre componentele din cluster, folosim comanda `kubectl get all`.

Dacă dorim să aflăm informații despre nodurile din cluster folosim următoarea comandă: `kubectl get nodes`. Pentru a vedea detalii despre nodurile dintr-un cluster folosim comanda `kubectl describe nodes <node>`.

Pentru ștergerea cluster-ului în care ne aflăm folosim comanda `kind delete cluster`.