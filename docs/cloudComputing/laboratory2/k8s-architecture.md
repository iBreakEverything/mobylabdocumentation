---
title: Arhitectura Kubernetes
sidebar_position: 3
---

<img alt="img" src="/img/cloud-computing/kubernetesarhitecture.png" width="75%" style={{margin: "auto", display: "block"}} />


Precum Docker Swarm, Kubernetes este un sistem format din mai multe [componente](https://kubernetes.io/docs/concepts/overview/components/#master-components):
1. **Kubectl** - CLI pentru configurarea clusterului și managementului aplicațiilor. Asemănător cu comanda docker
2. **Node** - nodul fizic din cadrul unui cluster
3. **Kubelet** - agentul (daemon) de Kubernetes care rulează pe fiecare nod
4. **Control Plane** - colecția de noduri care fac management la cluster. Include API Server, Scheduler, Controller Manager, CoreDNS, Etcd. Asemănător cu nodurile "master" din Docker Swarm.

<img alt="img" src="/img/cloud-computing/controlplane.png" width="75%" style={{margin: "auto", display: "block"}} />