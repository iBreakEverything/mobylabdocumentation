---
title: ConfigMaps și Secrets
sidebar_position: 11
---

Un ConfigMap este un obiect folosit pentru a stoca într-un format de tipul cheie-valoare date care nu sunt sensitive. Un pod poate consuma un ConfigMap ca o variabilă de mediu, ca un argument în linie de comandă sau ca un fișier de configurare într-un volum. Un astfel de obiect oferă opțiunea de a decupla configurația specifica unui mediu de imaginile de container și de codul aplicației, ceea ce sporește portabilitatea aplicațiilor.

Un exemplu simplu este separarea mediilor. Pentru development, o să folosim calculatorul local, iar pentru producție o să folosim un provider de cloud. Configurăm codul astfel încât acesta se conectează la o bază de date folosind o variabilă de mediu, de exemplu `DATABASE_HOST`. Pe mediul local o să setăm variabila de mediu la localhost (presupunând că avem un server de bază de date pe localhost), iar în cloud o să setăm variabila la un serviciu de Kubernetes prin care este expusă o bază de date (o să învățăm despre servicii în următorul laborator).

Un obiect de tipul ConfigMap nu este folosit pentru a stoca cantități mari de date. Pentru o dimensiune mai mare de 1MB se vor folosi volume (despre care învățăm în laboratorul următor).

Fișier pentru configurarea unui ConfigMap:
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
    name: game-demo
data:
    # property-like keys; each key maps to a simple value
    player_initial_lives: "3"
```

:::note Task
- aplicați ConfigMap-ul de mai sus, folosind comanda `apply`: `kubectl apply -f <configmapfile>`
- verificați că a fost creat obiectul: `kubectl get configmap`
:::

Un ConfigMap poate fi folosit într-un pod în mai multe moduri, nu doar ca în exemplul de mai sus, într-o variabilă de mediu. Mai există și opțiunea de a folosi un ConfigMap într-un volum (cum înca nu știm să lucrăm cu volume, o să învățăm în laboratorul următor).

Fișier pentru configurarea unui pod folosind ConfigMap (în acest exemplu folosind ConfigMap-ul definit anterior):
```yaml
apiVersion: v1
kind: Pod
metadata:
    name: configmap-demo-pod
spec:
    containers:
        - name: demo
          image: alpine
          command: ["sleep", "3600"]
          env:
            # Define the environment variable
            - name: PLAYER_INITIAL_LIVES # Notice that the case is different here
                                    # from the key name in the ConfigMap.
          valueFrom:
            configMapKeyRef:
                name: game-demo          # The ConfigMap this value comes from.
                key: player_initial_lives # The key to fetch.
```

:::note Task
Creați acest pod-ul definit mai sus.
:::

S-a creat în interiorul pod-ului o variabilă de mediu numită `PLAYER_INITIAL_LIVES` care o să își ia valoarea din cheia `player_initial_lives` al ConfigMap-ului nostru.

:::note Task
Pentru a verifica că totul a mers cum ne așteptam, afișați toate variabilele de mediu din interiorul podului proaspăt creat.
:::