---
title: Cloud computing
sidebar_position: 1
---

<img alt="img" src="/img/cloud-computing/CC.png" width="400" style={{margin: "auto", display: "block"}} />

# Despre

TODO

# Echipa

## Curs

- Master SSA: [Ciprian Dobre](mailto:ciprian.dobre@upb.ro)
- Master eGov: [Radu-Ioan Ciobanu](mailto:radu.ciobanu@upb.ro)

## Laborator

- [Florin Mihalache](mailto:florin.razvan.mihalache@gmail.com)
- [Teia Vava](mailto:teia_vava@yahoo.com)
- [Sergiu-Cristian Toader](mailto:sergiu.toader99@gmail.com)
- [Andrei Damian](mailto:andreidamian0612@gmail.com)
- [Bogdan Georgescu](mailto:bgeorgescu@upb.ro)

# Orar

TBD