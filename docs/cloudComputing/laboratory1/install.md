---
title: Instalare
sidebar_position: 4
---

Docker este disponibil în două variante: Community Edition (CE) și Enterprise Edition (EE). Docker CE este util pentru dezvoltatori și echipe mici care vor să construiască aplicații bazate pe containere. Pe de altă parte, Docker EE a fost creat pentru dezvoltare enterprise și echipe IT care scriu și rulează aplicații critice de business pe scară largă. Versiunea Docker CE este gratuită, pe când EE este disponibilă cu subscripție. În cadrul laboratorului de Cloud Computing, vom folosi Docker Community Edition. Docker este disponibil atât pe platforme desktop (Windows, macOS), cât și Cloud (Amazon Web Services, Microsoft Azure) sau server (CentOS, Fedora, Ubuntu, Windows Server 2016, etc.).

## Linux
Comenzile de mai jos sunt pentru Ubuntu. Pentru alte variante de Linux (Debian, CentOS, Fedora), găsiți informații suplimentare pe pagina de documentație oficială Docker.

Pentru instalarea Docker CE, este nevoie de una din următoarele versiuni de Ubuntu: Focal 20.04 (LTS), Disco 19.04, Cosmic 18.10, Bionic 18.04 (LTS), Xenial 16.04 (LTS). Docker CE are suport pentru arhitecturile ***x86_64***, ***amd64***, ***armhf***, ***arm64***, ***s390x*** (IBM Z) și ***ppc64le*** (IBM Power).

Varianta recomandată de instalare a Docker CE presupune folosirea repository-ului oficial, deoarece update-urile sunt apoi instalate automat. La prima instalare a Docker CE pe o mașină, este necesară inițializarea repository-ului (exemplul de mai jos este pentru un sistem cu arhitectură ***amd64***):

```shell
$ sudo apt-get update
```

```shell
$ sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
```

```shell
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

```shell
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
```

După inițializare, se poate instala Docker CE:

```shell
$ sudo apt-get update
```

```shell
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

:::tip Sfat
O variantă mai simplă de a instala Docker CE pe Linux este utilizarea [acestui](https://get.docker.com/) script.
:::


## Windows și MacOS
Pentru că Docker nu avea inițial suport nativ pentru Windows și MacOS, s-a introdus [Docker Toolbox](https://docs.docker.com/toolbox/overview/), care poate lansa un mediu Docker virtualizat (mai precis, se folosește o mașină virtuală VirtualBox pentru a fi baza mediului de Docker). Recent, Docker Toolbox a fost marcat ca "legacy" și a fost înlocuit de [Docker Desktop for Mac](https://docs.docker.com/docker-for-mac/) și [Docker Desktop for Windows](https://docs.docker.com/docker-for-windows/), care oferă funcționalități similare cu performanțe mai bune. Mai mult, Windows Server 2016 și Windows 10 au acum suport pentru Docker nativ pentru arhitecturi ***x86_64***.