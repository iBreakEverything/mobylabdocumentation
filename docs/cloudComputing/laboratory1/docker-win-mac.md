---
title: Docker pe Windows și pe MacOS
sidebar_position: 10
---

Docker a fost creat nativ pentru Linux, utilizând componente de kernel specifice Linux, cum ar fi ***cgroups*** sau ***namespaces***, folosite pentru a izola procese și alte componente ale sistemului de operare. Începând din 2016, el poate rula nativ și pe Windows, dar doar pentru versiunile Windows Server 2016 și Windows 10. De aceea, pentru a rula pe un sistem de operare desktop precum un Windows mai vechi sau MacOS, Docker necesită rularea virtualizată.

Pe Windows, se folosește izolare Hyper-V pentru a rula un kernel Linux cu un set minimal de componente suficiente pentru a executa Docker. Pe MacOS, Docker for Mac este o aplicație nativă care conține un hypervizor bazat pe [xhyve](https://github.com/machyve/xhyve) și o distribuție minimală de Linux, peste care rulează Docker. Astfel, se oferă o experiență mult mai apropiată de utilizarea Docker pe Linux, sistemul de operare pentru care a fost creat.

Ca un exemplu, pentru a avea acces în mașina virtuală de Docker pentru MacOS, se poate folosi ***screen*** (pentru a se termina sesiunea, se folosește combinația de taste ***Ctrl+a, k***):
```shell
$ screen /Users/<UID>/Library/Containers/com.docker.docker/Data/vms/0/tty
 
linuxkit-025000000001:~# pwd
/root
```

O alternativă mai elegantă la comanda de ***screen*** de mai sus este utilizarea unei imagini speciale pentru accesul în mașina virtuală Docker:
```shell
$ docker run -it --privileged --pid=host justincormack/nsenter1
```