---
title: Instrumente pentru dezvoltarea programelor
sidebar_position: 1
---

<img alt="img" src="/img/softwareDevelopment/idp.png" width="400" style={{margin: "auto", display: "block"}} />

## Despre

Cursul are ca scop însușirea cunoștințelor legate de folosirea instrumentelor pentru dezvoltarea programelor și formarea deprinderilor practice pentru analiza și alegerea soluțiilor potrivite pentru crearea de produse software de complexitate ridicată. Acesta urmărește, de asemenea, dobândirea unui fundament solid (teorie și aplicatii practice) privind instrumentele avansate de dezvoltare a programelor. Cursul include o introducere asupra mediilor, limbajelor, instrumentelor moderne de dezvoltare a programelor, prezintă caracteristicile unor medii de dezvoltare de ultimă generație, noțiuni privind programarea interfețelor cu utilizatorii, tehnologiile existente ce permit dezvoltarea programelor ce interfațează cu baze de date, componente și aplicații web. După absolvirea cursului, studentul va putea să proiecteze soluții corecte de aplicații folosind componente software existente și integrarea în diverse arhitecturi de dezvoltare, să dezvolte un sistem prin integrarea de diverse componente și să analizeze proprietățile sistemului, și să evalueze critic cele mai relevante tehnologii existente în contextul posibilei integrări a acestora în diverse sisteme proiectate. La laborator, se studiază Docker și Docker Swarm ca unelte de dezvoltare a aplicațiilor portabile bazate pe microservicii.

## Echipa

### Curs

- Cătălin Goșman

### Laborator

- Radu Ciobanu
- Andrei Damian
- Ioana Marin
- Florin Mihalache
- Ștefan Olteanu
- Alin Pătrașcu

## Orar

### Curs

* Marți 8-10 (AN030)

### Laborator

* Luni 8-10 (PR606), Andrei Damian
* Luni 16-18 (PR606), Ștefan Olteanu
* Luni 18-20 (PR606), Florin Mihalache
* Marți 16-18 (PR606), Alin Pătrașcu
* Marți 18-20 (PR606), Ioana Marin
* Miercuri 12-14 (PR606), Radu Ciobanu

## Calendar

<div style={{textAlign: "center"}}><iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRNJc1xy-jZYalK4ArqWsQrwa1gkkNzoxRa2RN5XDaz_o1yck1SWduhpsEMN6kdShRn44W14hKeyvA-/pubhtml?gid=1339154378&amp;single=true&amp;widget=true&amp;headers=false" frameBorder="0" width="100%" height="500"></iframe></div>