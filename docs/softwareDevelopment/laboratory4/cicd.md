---
title: GitLab CI/CD
sidebar_position: 2
---

Conceptul de <em>**CI/CD**</em> se referă la:

- continuous integration - integrarea automată în sistem a modificărilor de cod
- continuous deployment - plasarea automată a codului modificat în testare/producție.

Acest concept se mulează natural pe filozofia microserviciilor, unde o aplicație este
„spartă” în mai multe <em>**module separate și independente**</em>. Pe măsură ce
codul unui modul este actualizat, acesta este integrat automat în sistem, fără să
perturbe execuția celorlalte module.

În acest laborator, se exemplifică procesul de CI/CD folosind GitLab. GitLab CI/CD se
bazează pe două componente:

- [GitLab Runners](https://docs.gitlab.com/runner/) - procese care execută
pipeline-uri
- <em>**.gitlab-ci.yml**</em> - fișier YAML de configurații declarative, care descrie
ce face fiecare etapă dintr-un pipeline.

:::tip
Un runner execută un pipeline. Un pipeline este format din etape. Fiecare etapă este
descrisă în fișierul de configurație <em>**.gitlab-ci.yml**</em>.
:::

## Structura codului sursă

Pentru a putea folosi conceptul de CI/CD cu GitLab cat mai eficient, se recomandă ca
fiecare microserviciu să se afle în propriul său <em>**repository**</em>, iar toate
repository-urile să fie grupate într-un <em>**grup**</em>. Așadar, pentru acest
laborator, vom avea următoarele repository-uri:

- <em>**IOService**</em> - conține codul pentru microserviciul IO implementat la
laboratoarele anterioare
- <em>**BooksService**</em> - conține codul pentru microserviciul de cărți
implementat la laboratoarele anterioare
- <em>**Configs**</em> - conține fișierele de configurare necesare rulării stivei de
servicii.

Codul este accesibil pe
[repo-ul oficial al laboratorului](https://gitlab.com/mobylab-idp).

## GitLab Runners

GitLab Runners sunt procese care execută pipeline-uri. Atunci când se dă comanda
<em>**git push**</em>, este lansat în execuție un pipeline aferent repository-ului
respectiv (de aici recomandarea de a avea un repository per serviciu).

Acestea vin în mai multe forme, însă modul cel mai facil de a lansa un runner în
execuție este sub formă de containere Docker.

### Configurare

Pentru a folosi un runner, este nevoie, în primul rând, să se acceseze pagina
repository-ului sau a grupului GitLab.

:::tip
Un runner de grup va putea rula pipeline-uri pentru fiecare repository din grupul
respectiv. Un runner de repository va putea rula pipeline-uri doar pentru acel
repository.
:::

Se intră în meniul de CI/CD al paginii de proiect și apoi se selectează opțiunea
„Expand” din dreptul „Runners”.

<img alt="img" src="/img/softwareDevelopment/lab4_cicd.png" width="300" style={{margin: "auto", display: "block"}} />

<img alt="img" src="/img/softwareDevelopment/lab4_runners.png" width="1200" style={{margin: "auto", display: "block"}} />

### GitLab Runners în Docker

Configuraera unui runner folosind Docker este simplă și necesită
[trei pași](https://docs.gitlab.com/runner/install/docker.html):

- instalarea
- înregistrarea
- modificarea fișierului de configurație <em>**config.toml**</em>.

Pentru instalare, se rulează următoarea comandă:

```shell showLineNumbers
$ docker run -d --name gitlab-runner --restart always -v /srv/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
```

:::caution
Runner-ul va rula în modul bind mount. Calea de pe gazdă dată runner-ului (în cazul
de față, <em>**/srv/gitlab-runner/config**</em>) trebuie să existe. În ea vor fi
reținute configurațiile runner-ului din interiorul containerului. În mod similar, se
poate folosi un volum.
:::

Pentru înregistrare, se rulează următoarea comandă și se urmează pașii specificați:

```shell showLineNumbers
$ docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
```

:::tip
Token-ul de înregistrare este cel din pagina grupului de GitLab.
:::

:::tip
Trebuie ținut minte ce se specifică la tag, deoarece tag-ul runner-ului va fi folosit
în cadrul script-ului <em>**.gitlab-ci.yml**</em>.
:::

:::tip
Atunci când se cere imaginea de Docker, se poate specifica <em>**docker:19.03**</em>.
:::

:::caution
Trebuie specificată aceeași cale de bind mount ca la comanda de instalare.
:::

Runner-ul de GitLab care rulează în Docker se bazează pe conceptul
<em>**DinD**</em> („Docker in Docker”). Pentru anumite operații, este nevoie de acces
elevat asupra sistemului Docker din gazdă. Așadar, trebuie făcute două modificări
asupra fișierului de configurație <em>**config.toml**</em>.

:::tip
Fișierul <em>**config.toml**</em> se găsesște la calea specificata în comanda de
instalare de la etapa 1.
:::

```toml showLineNumbers
concurrent = 1
check_interval = 0
 
[session_server]
  session_timeout = 1800
 
[[runners]]
  name = "IDP lab 4 runner"
  url = "https://gitlab.com/"
  token = "jEzCz9PACYL4Y1FB8vs2"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:19.03"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0
```

Modificările necesare sunt următoarele:

- <em>**privileged**</em> trebuie să fie setat pe <em>**true**</em>
- la volume, trebuie adăugat și
<em>**/var/run/docker.sock:/var/run/docker.sock**</em>.

Dupa ce se efectuează modificările, se execută următoarea comandă:

```shell showLineNumbers
$ docker restart gitlab-runner
```

### Script-ul de pipeline

Script-ul <em>**.gitlab-ci.yml**</em> descrie execuția unui pipeline pe un runner.
Puteți observa un astfel de script mai jos.

```yaml showLineNumbers
docker-build-master:
  stage: build
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"
  only:
    - master
  tags:
    - idp
    - lab4

deploy-service-master:
  stage: deploy
  script:
    - apk add --update curl
    - curl -XPOST http://192.168.99.126:9000/api/webhooks/e37c80b1-9315-49d2-b0ad-5b3d8dade98e
  only:
    - master
  tags:
    - idp
    - lab4
```

:::tip
Trebuie câte un astfel de script <em>**.gitlab-ci.yml**</em> pentru fiecare
repository.
:::

Script-ul prezentat mai sus descrie două etape ale pipeline-ului:

- <em>**build**</em> - codul este construit într-o imagine de Docker și salvat
într-un registru
- <em>**deploy**</em> - serviciul de Docker este încărcat in cluster-ul de Swarm,
utilizând un webhook Portainer.

:::tip
Un webhook se poate genera doar după ce serviciul rulează deja în Swarm.
:::

Un pipeline se poate observa din GitLab mergând la meniul „CI/CD” al unui repository,
la opțiunea „Pipelines”.

<img alt="img" src="/img/softwareDevelopment/lab4_pipelines.png" width="300" style={{margin: "auto", display: "block"}} />

<img alt="img" src="/img/softwareDevelopment/lab4_pipelines2.png" width="1200" style={{margin: "auto", display: "block"}} />

Un exemplu de fișier <em>**.gitlab-ci.yml**</em> funcțional poate fi găsit
[aici](https://gitlab.com/mobylab-idp/booksservice/-/blob/master/.gitlab-ci.yml).