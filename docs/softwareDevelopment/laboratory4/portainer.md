---
title: Portainer
sidebar_position: 1
---

[Portainer](https://www.portainer.io/) este o platformă Web care permite
administrarea unui cluster Docker Swarm. Instrucțiunile oficiale se pot găsi
[aici](https://docs.portainer.io/start/install-ce).

<img alt="img" src="/img/softwareDevelopment/lab4_dashboard.png" width="1200" style={{margin: "auto", display: "block"}} />

Portainer poate rula ca un container separat sau ca serviciu de Swarm.

:::tip
<em>**Vă recomandăm să îl rulați ca serviciu de Swarm**</em>. În cazul în care
rulează ca serviciu de Swarm, Portainer are două componente: aplicația Web
propriu-zisă, și un agent care rulează în mod global (pe toate nodurile).
:::

Un exemplu de fișier Docker Compose de stivă de servicii pentru Portainer poate fi
observat mai jos.

```yaml showLineNumbers
version: '3.2'

services:
  agent:
    image: portainer/agent:2.11.1
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /var/lib/docker/volumes:/var/lib/docker/volumes
    networks:
      - agent_network
    deploy:
      mode: global
      placement:
        constraints: [node.platform.os == linux]

  portainer:
    image: portainer/portainer-ce:2.11.1
    command: -H tcp://tasks.agent:9001 --tlsskipverify
    ports:
      - "9443:9443"
      - "9000:9000"
      - "8000:8000"
    volumes:
      - portainer_data:/data
    networks:
      - agent_network
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints: [node.role == manager]

networks:
  agent_network:
    driver: overlay
    attachable: true

volumes:
  portainer_data:
```

Dashboard-ul Portainer va fi accesibil pe portul 9000. La prima accesare, se setează
un utilizator administrator cu o parolă.

Pe lângă utilitatea oferită de posibilitatea gestiunii cluster-ului prin interfața
vizuală, Portainer oferă <em>**webhook-uri**</em> de CI/CD. Un webhook este un
endpoint care, atunci când este accesat, execută o acțiune. În cazul Portainer,
webhook-urile vor actualiza serviciile de Docker. Pentru a genera un webhook, se
intră pe pagina serviciului și se face toggle pe „Service Webhook”, așa cum se
observă în imaginea de mai jos.

<img alt="img" src="/img/softwareDevelopment/lab4_webhook.png" width="1200" style={{margin: "auto", display: "block"}} />

:::caution
Pentru a crea un webhook, trebuie mai întâi ca serviciul să ruleze în cadrul Docker
Swarm.
:::

De asemenea, Portainer vă facilitează interacțiunea cu Docker Swarm fără a fi nevoie
de a da comenzi în terminal, cum ar fi gestionarea secretelor.

<img alt="img" src="/img/softwareDevelopment/lab4_portainersecret.png" width="1200" style={{margin: "auto", display: "block"}} />

Nu doar atât, dar, ca să aveți control complet asupra serviciilor ce pot rula în
Swarm, puteți să lansați stiva de servicii direct din editorul de YML din secțiunea
„Stacks”.

<img alt="img" src="/img/softwareDevelopment/lab4_portainereditor.png" width="1200" style={{margin: "auto", display: "block"}} />

Ca Portainer să poată să vă acceseze registrele private, puteți să adăugați acele
registre de imagini în secțiunea „Registries” cu utilizatorul vostru și parola/un
token de acces.

<img alt="img" src="/img/softwareDevelopment/lab4_portainerregistry.png" width="1200" style={{margin: "auto", display: "block"}} />