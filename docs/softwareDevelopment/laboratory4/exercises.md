---
title: Exerciții
sidebar_position: 3
---

1. Pornind de la structura din
[repo-ul oficial al laboratorului](https://gitlab.com/mobylab-idp), creați un grup cu
trei repository-uri (<em>**BooksService**</em>, <em>**Configs**</em> și
<em>**IOService**</em>).
2. Modificați fișierul <em>**stack-kong.yml**</em> pentru a folosi imagini din
registrul vostru pentru serviciile de business logic și IO.
3. Porniți o stivă de servicii pentru aplicația de bibliotecă pe baza fișierului
Docker Compose modificat.
4. Pe baza exemplului de fișier Docker Compose pentru Portainer de mai sus, porniți o
stivă de servicii Portainer.
5. Din Portainer, generați un webhook pentru serviciul de business logic.
6. Instalați și configurați un GitLab Runner.
7. Adăugați un script <em>**.gitlab-ci.yml**</em> în repository-ul de business logic
cu o etapă de <em>**build**</em> și una de <em>**deploy**</em>, conform modelului din
laborator.
8. Faceți un push din repository-ul de business logic și verificați că stiva de
servicii se actualizează corespunzător.