---
title: Exerciții
sidebar_position: 3
---

## Docker Compose

:::tip
Puteți accesa resursele practice pentru acest laborator pe
[repository-ul oficial](https://gitlab.com/mobylab-idp/laborator-2) al materiei IDP.
Exercițiile urmăresc înțelegerea conceptelor de Docker Compose.
:::

1. Scrieți configurația containerelor de la ultimul set de exerciții din
[laboratorul 1](https://mobylab.docs.crescdi.pub.ro/docs/softwareDevelopment/laboratory1/exercises) în maniera Compose:
   - pentru containerul de API, folosiți sursele și Dockerfile-ul din directorul
   <em>**API**</em> aflat pe repository, sau direct imaginea
   <em>**mobylab/idp-laborator2-api**</em> de pe Docker Hub
   - pentru containerul de Postgres, găsiți fișierul <em>**init-db.sql**</em>
   în directorul <em>**Database**</em> din repository.
2. Introduceți un al treilea serviciu bazat pe imaginea <em>**adminer**</em>,
care face parte dintr-o rețea comună cu baza de date, alta decât cea pe care baza
de date o folosește la comunicația cu API-ul (așadar, trebui să aveți două rețele).
3. Spargeți configurația în două fișiere de Compose, unul de bază și unul care să
folosească secrete (găsiți fișierele pentru secrete în directorul <em>**secrets**</em>
din repository). Rulați cele două configurații împreună.

:::caution
După ce veți sparge configurația inițială în două, o să observați că trebuie să
renunțați la niște variabile de mediu ale bazei de date din fișierul de bază, și
anume <em>**POSTGRES_USER**</em> și <em>**POSTGRES_PASSWORD**</em>. În configurația cu
secrete, numele de utilizator și parola vor fi setate prin variabilele de mediu
<em>**POSTGRES_USER_FILE**</em> și <em>**POSTGRES_PASSWORD_FILE**</em>, care sunt
disjuncte cu cele menționate anterior. Este important de menționat că valorile pentru
aceste două variabile de mediu reprezintă căile complete ale fișierelor secret, de
forma <em>**/run/secrets/NUME_SECRET**</em>.
:::

:::caution
Pentru a introduce secrete și în serviciul de API, trebuie să setați variabila de
mediu <em>**NODE_ENV**</em> la valoarea „staging” și să setați variabilele de mediu
<em>**PGPASSWORD_SECRET**</em> și <em>**PGUSER_SECRET**</em> doar cu numele
secretului, nu cu toată calea <em>**/run/secrets/NUME_SECRET**</em> precum în cazul
bazei de date.
:::

## Docker Swarm

Pentru exercițiile din această secțiune, vom lucra cu o variantă extinsă a aplicației
bibliotecă de mai sus. Astfel, codul (care se găsește pe
[repository-ul oficial IDP](https://gitlab.com/mobylab-idp/laborator3)) este format
din trei mici microservicii scrise în NodeJS și un script de inițializare pentru o
bază de date PostgreSQL. Cele trei microservicii sunt:

- <em>**ApiGateway**</em> - mediază accesul dinspre lumea exterioară și
redirecționează cererile HTTP către serviciul de cărți
- <em>**Books**</em> - se ocupă de partea de „business logic” ce ține de cărți și
apelează serviciul IO pentru date
- <em>**IO**</em> - gestionează datele sistemului și realizează comunicația cu baza de
date.

### Scop exerciții

Scopul exercițiilor este ca, plecând de la codul propus, să realizați o stivă de
servicii care să funcționeze corect. Funcționalitatea este reprezentată de trei
acțiuni:

- adăugarea unei cărți în bibliotecă
- vizualizarea informațiilor despre toate cărțile din bibliotecă
- vizualizarea detaliilor unei cărți.

Pe scurt, va trebui să scrieți unul sau mai multe fișiere Docker Compose pe care
să le folosiți pentru a rula configurația sub forma unei stive de servicii Docker
Swarm. Stiva creată va fi formată din 5 servicii:

- <em>**gateway**</em> - puteți să creați imaginea pe baza surselor din repository, sau puteți folosi imaginea <em>**mobylab/idp-laborator2-gateway**</em>
- <em>**books-service**</em> - puteți să creați imaginea pe baza surselor din repository, sau puteți folosi imaginea <em>**mobylab/idp-laborator2-books**</em>
- <em>**io-service**</em> - puteți să creați imaginea pe baza surselor din repository, sau puteți folosi imaginea <em>**mobylab/idp-laborator2-io**</em>
- <em>**db**</em> - folosiți imaginea <em>**postgres:12**</em>
- <em>**adminer**</em> - folosiți imaginea adminer.

### Variabile de mediu

Variabilele de mediu ce vor trebui puse in fișierul Docker Compose sunt următoarele:

- serviciul <em>**gateway**</em>:
   - BOOKS_SERVICE_API_ROUTE: books-service/api
   - NODE_ENV: development
- serviciul <em>**books-service**</em>:
   - IO_SERVICE_API_ROUTE: io-service/api
   - NODE_ENV: development
- serviciul <em>**io-service**</em>:
   - PGUSER: admin
   - PGPASSWORD: admin
   - PGHOST: db
   - PGPORT: 5432
   - PGDATABASE: books
   - NODE_ENV: development
- serviciul <em>**db**</em>:
   - POSTGRES_DB: books
   - POSTGRES_USER: admin
   - POSTGRES_PASSWORD: admin.

### Rețele
Pentru a izola accesul serviciilor asupra clusterului, sunt necesare mai multe
rețele, astfel:

- serviciul <em>**gateway**</em> va comunica doar cu serviciul
<em>**books-service**</em>
- serviciul <em>**books-service**</em> va comunica atât cu serviciul de
<em>**gateway**</em>, cât și cu <em>**io-service**</em>
- serviciul <em>**io-service**</em> va comunica atât cu serviciul
<em>**books-service**</em>, cât și cu serviciul <em>**db**</em>
- serviciul <em>**db**</em> va comunica atât cu serviciul <em>**io-service**</em>,
cât și cu <em>**adminer**</em>.

### Volume și bind mounts

Configurația de volume și bind mounts este la fel ca cea de la exercițiile de mai sus.
Astfel, serviciul de bază de date are nevoie de un volum pentru persistență și de un
bind mount pentru a se injecta scriptul de inițializare.

### Porturi publice
Singurele servicii accesibile din exterior vor fi <em>**gateway**</em> și
<em>**adminer**</em>. Pentru <em>**gateway**</em>, trebuie expus portul intern 80,
peste care se mapează alt port (de exemplu, 3000), iar pentru <em>**adminer**</em>
trebuie expus portul intern 8080 (peste care se poate de asemenea mapa alt port
extern).

### Rute HTTP
Dacă reușiți să rulați configurația, puteți să o testați folosind cURL sau
[Postman](https://learning.postman.com/docs/getting-started/sending-the-first-request/)
pe următoarele rute:

- <em>**POST localhost:PORT_PUBLIC/api/books**</em> cu corpul JSON
`{"title": "Harry Potter and the Prisoner of Azkaban", "author": "J.K. Rowling", "genre": "Fantasy"}`
- <em>**GET localhost:PORT_PUBLIC/api/books**</em>
- <em>**GET localhost:PORT_PUBLIC/api/books/ID_CARTE**</em>.

:::tip
Pentru a adăuga un corp JSON unei cereri POST in Postman, selectați Body → Raw → JSON
și scrieți JSON-ul aferent.
:::

### Secrete

Opțional, puteți adăuga secrete aplicației, pentru numele de utilizator și parola
necesare logării în baza de date. Astfel, serviciile care vor avea nevoie de secrete
sunt <em>**io-service**</em> și <em>**db**</em>. În cele două servicii, va trebui să
sufixați variabilele de mediu destinate numelui de utilizator și parolei cu
<em>**_FILE**</em> și să puneți valorile acestor variabile de mediu în funcție de
documentație:

- pentru <em>**io-service**</em>, la variabilele de mediu pentru numele de
utilizator și parolă se pune doar numele secretului, iar <em>**NODE_ENV**</em>
se setează la <em>**staging**</em>
- pentru serviciul <em>**db**</em>, la variabilele de mediu se pune toată calea
secretului (<em>**/run/secrets/nume-secret**</em>).

### Enunțuri

1. Scrieți un fișier Docker Compose care să folosească un build local și rulați pe
mașina voastră
2. Construiți imaginile și urcați-le pe Docker Hub sau orice alt registru
3. Scrieți un fișier Docker Compose care să folosească imaginile din registru și
rulați pe mașina voastră
4. Adăugați elemente de Docker Swarm la serviciile din fișierul Docker Compose (la
proprietatea <em>**deploy**</em>) și rulați stiva de servicii într-un cluster
(folosind Play with Docker sau Docker Machine)
5. Modificați fișierul Docker Compose ca să utilizați secrete externe pentru numele de
utilizator și parolă în containerele de IO și bază de date.