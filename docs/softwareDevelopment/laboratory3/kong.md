---
title: Kong API Gateway
sidebar_position: 2
---

În cadrul scheletului de cod din
[laboratorul 2](https://mobylab.docs.crescdi.pub.ro/docs/softwareDevelopment/laboratory2/exercises),
este implementat un API Gateway minimal sub forma de server de NodeJS. Acesta are
următoarele funcționalități:

- expune rute din aplicație
- funcționează ca un punct central de acces în sistem.

Cu toate acestea, implementarea programatică a unui API Gateway necesită timp și, de multe
ori, munca depusă implică scriere repetitivă de cod.

[Kong](https://konghq.com/) este unul dintre liderii din domeniul Cloud
în materie de soluții de API Gateway declarative. Kong este nativ Docker, este bazat pe
Nginx si nu necesită scriere de cod, ci doar furnizarea unei configurații (sau
configurarea on-the-fly prin cereri HTTP).

Kong funcționează ca un serviciu de Docker și interceptează traficul venit, în funcție de
regulile menționate, și apoi îl redirectează către alte servicii.

În plus, Kong oferă o gamă variată de <em>**plugins**</em> ce aduc funcționalități
adiționale precum rate limiting, compression, logging, etc.

## Arhitectura implementată

Arhitectura este identică cu cea a exercițiului din
[laboratorul 2](https://mobylab.docs.crescdi.pub.ro/docs/softwareDevelopment/laboratory2/exercises),
însă, în loc de API Gateway programatic, vom folosi Kong. În plus, vom expune și
serviciul Adminer tot prin intermediul Kong.

<img alt="img" src="/img/softwareDevelopment/lab3_arch.png" width="700" style={{margin: "auto", display: "block"}} />

Comportamentul dorit va fi ca, în urma apelului <em>**IP_PUBLIC/api/books**</em>, să
interacționăm cu serviciul Books (POST/GET) și, în urma apelului
<em>**IP_PUBLIC/adminer**</em>, să interacționăm cu serviciul Adminer.

## Implementare Kong

Kong poate fi rulat atât în modul <em>**DB-on**</em>, cât și în modul <em>**DB-less**</em>.
Noi vom folosi abordarea <em>**DB-less**</em>, furnizând o configurație bazată pe un fișier
<em>**.yml**</em>. În modul <em>**DB-on**</em>, Kong mai folosește o bază de date și
configurația se face pe bază de cereri POST către API-ul intern de configurare.

### Fișierul de configurație

```yaml showLineNumbers
_format_version: "2.1"

services:
  - name: books-service
    url: http://labkong_books-service/api
    routes:
      - name: books-service-route
        paths: 
          - /api/books
          
  - name: db-adminer
    url: http://labkong_adminer:8080
    routes:
      - name: adminer-service
        paths:
          - /adminer

consumers:
  - username: lab-student

plugins:
  - name: key-auth
    service: books-service

keyauth_credentials:
  - consumer: lab-student
    key: mobylab
    hide_credentials: true
```

- <em>**services**</em> - reprezintă ce servicii se vor conecta la Kong pentru
interceptare și forwarding de trafic
- <em>**consumers**</em> - reprezintă ce opțiuni se pot seta fiecărui serviciu, în funcție
de plugin (este opțional și se folosește doar în contextul pluginurilor).

:::tip
Fiecare serviciu trebuie să aibă obligatoriu definite următoarele: <em>**name**</em>,
<em>**url**</em>, <em>**routes**</em>.
:::

- <em>**services → name**</em> - numele serviciului din punct de vedere al configurării
rezultate, trebuie să fie unic
- <em>**services → url**</em> - calea de comunicare cu serviciul de Docker; în cazul nostru,
reprezintă ruta din interioriul serviciilor de Docker unde se vor forwarda cererile HTTP.

:::tip
Observați că în URL se precizează <em>**NUME-STIVA_NUME_SERVICIU_DOCKER**</em> în loc de IP.
:::

- <em>**services → routes**</em> - rutele pe care se va mapa traficul din exterior
- <em>**services → routes → name**</em> - numele rutei din punct de vedere al configurării
rezultate, trebuie să fie unic
- <em>**services → routes → path**</em> - calea de comunicare din exterior, care se va mapa
pe ruta furnizată în URL.

:::tip
Plugin-urile sunt opționale, însă am vrut să demonstrăm utilizarea unui plugin de autorizare
pe bază de cheie de API trimisă în header. Deoarece pluginul este activat doar pe serviciul
Books, numai calea <em>**/api/books**</em> este securizată.
:::

### Includerea în stiva de servicii Docker

Ca oricare alt serviciu de Docker, Kong se adaugă în fișierul de configurație al stivei:

```yaml showLineNumbers
services:
  kong:
    image: kong:latest
    volumes:
      - ./kong:/usr/local/kong/declarative # injectarea fișierului de configurare la calea specificată
    environment:
      KONG_DATABASE: 'off' # obligatoriu, dacă se vrea modul DB-less
      KONG_DECLARATIVE_CONFIG: /usr/local/kong/declarative/kong.yml # trebuie specificat unde anume se va găsi fișierul de configurare
      KONG_PROXY_ACCESS_LOG: /dev/stdout
      KONG_ADMIN_ACCESS_LOG: /dev/stdout
      KONG_PROXY_ERROR_LOG: /dev/stderr
      KONG_ADMIN_ERROR_LOG: /dev/stderr
      KONG_ADMIN_LISTEN: 0.0.0.0:8001, 0.0.0.0:8444 ssl
    ports:
      - 8000:8000 # expunerea porturilor
      - 8443:8443
    deploy:
      placement:
        constraints: [node.role == manager] # constrângerea de rulare doar pe manager, pentru a nu exista conflict la nivel de volume
    networks:
      - internal
```

:::tip
Aveți grija să conectați toate serviciile pe care le vreți expuse în Kong în aceeași rețea
cu el. Observați că, în cadrul configurației de mai sus, am folosit rețeaua
<em>**internal**</em>. Atât serviciul Books cât și Adminer vor trebui să fie în
rețeaua <em>**internal**</em>.
:::

:::tip
Kong ascultă pe porturile 8000 și 8443. Pentru a expune în public 80 și 443 (porturile
standard pentru HTTP și HTTPS), este nevoie de mapare explicită.
:::

## Comportament așteptat

Dacă totul a decurs bine, apelarea rutei <em>**IP_PUBLIC_PLAY_WITH_DOCKER/adminer**</em>
va rezulta în panoul de administrare a bazei de date, iar cererile din Postman pe ruta
<em>**IP_PUBLIC_PLAY_WITH_DOCKER/api/books**</em> vor eșua în cazul în care nu există o
cheie API în header, sau să funcționeze dacă se pune cheia <em>**mobylab**</em> în header.

<img alt="img" src="/img/softwareDevelopment/lab3_apikey.png" width="700" style={{margin: "auto", display: "block"}} />

## Extindere Kong

Deși exemplul arătat anterior este unul simplu. Kong se poate folosi pentru mult mai multe
lucruri utile în lucrul cu servicii distribuite și microservicii, precum rate limiting,
distributed logging, IP restriction, 3rd party authorization, CORS, etc.

Esențial, Kong se folosește ca un punct central de acces în sistem, care se introduce nativ
în ecosistemul Docker. În plus, este indicat să dedicați cât mai multe funcționalități care
au legătură cu traficul Web către API Gateway, pentru a nu îngreuna business logic-ul din
interiorul serviciilor.

În continuare, sa va ilustra aplicarea următoarelor pluginuri:

- [Rate Limiting](https://docs.konghq.com/hub/kong-inc/rate-limiting/) - limitarea numărului
de cereri pe anumite rute
- [JWT](https://docs.konghq.com/hub/kong-inc/jwt/) - autorizarea serviciilor pe baza unui JWT
- [CORS](https://docs.konghq.com/hub/kong-inc/cors/) - adaugărea politicilor CORS pe anumite
servicii
- [Bot Detection](https://docs.konghq.com/hub/kong-inc/bot-detection/) - protecția împotriva
boților web
- [Prometheus Logging](https://docs.konghq.com/hub/kong-inc/prometheus/) - suport pentru
extragerea logurilor utilizând Prometheus.

```yaml showLineNumbers
_format_version: "2.1"

services:
  - name: books-service
    url: http://books-service/api
    routes:
      - name: books-service-route
        paths: 
          - /api/books
          
  - name: db-adminer
    url: http://adminer:8080
    routes:
      - name: adminer-service
        paths:
          - /adminer

consumers:
  - username: lab-student
    
plugins:
  - name: rate-limiting
    config: 
      minute: 10
      limit_by: ip
      policy: local

  - name: cors
    service: books-service
    config:
      origins: 
       - "*"

  - name: bot-detection

  - name: prometheus

  - name: jwt
    service: books-service
    # config:
    #   claims_to_verify:
    #     - exp
    #   maximum_expiration: 60

jwt_secrets:
  - consumer: lab-student
    key: Moby Labs IDP
    algorithm: HS256
    secret: mobylab
```

## Logging

Fișierul <em>**docker-stack-kong-plugins.yml**</em> din
[repository-ul laboratorului](https://gitlab.com/mobylab-idp/laborator3) integrează, pe
lângă Kong-ul extins cu plugin-uri, și Prometheus și Grafana pentru explorarea logurilor
provenite din Kong.

Pentru a accesa Grafana, este nevoie să realizați următorii pași:

1. intrați pe <em>**IP:3000**</em>
2. conectați-vă cu <em>**admin**</em> / <em>**admin**</em>
3. setați sursa de date <em>**http://prometheus:9090**</em>
4. importați [dashboard-ul](https://grafana.com/grafana/dashboards/7424) oferit de Kong.