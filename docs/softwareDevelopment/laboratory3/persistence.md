---
title: Persistență în Docker Swarm
sidebar_position: 1
---

În contextul Docker Swarm, nu mai există o singură gazdă, ci multiple noduri conectate în
rețea. Acest lucru face ca folosirea volumelor locale să nu fie recomandată, deoarce
orchestrarea unui task pe un nod care nu are volumul cerut de container va rezulta în
respingerea acelui task.

Există două posibilități principale de rezolvare a acestei probleme. Prima variantă
presupune constrângerea unor servicii astfel încât să ruleze doar pe noduri manager, astfel:

```yaml showLineNumbers
services:
    example:
        deploy:
            placement:
                constraints: [node.role == manager]
```

Această practică este utilă doar atunci când există un singur nod manager și astfel suntem
siguri că task-urile ale căror containere au nevoie de volumele definite în configurație vor
rula doar pe managerul unde exista volumele.

În cazul în care există mai multe noduri manager și dorim, de exemplu, să rulăm o bază de
date într-un serviciu, dacă aceasta va fi orchestrată să ruleze pe alt nod față de cel pe
care a fost prima oară orchestrată, nu va mai avea acces la volumul unde a stocat date
inițial. În acest caz, va crea un volum nou pe noul nod, ajungându-se astfel la
inconsistență.

Cea de-a doua variantă pentru a rezolva problema descrisă mai sus este configurarea
volumelor astfel încât să folosească un sistem de stocare extern (cum ar fi NFS, GlusterFS,
Samba, Amazon S3, etc.).

:::caution
Din păcate, infrastructura de Play with Docker nu permite realizarea unui demo practic
care să ilustreze folosirea volumelor cu stocare externă, având limitări la nivel de
comunicație inter-mașini. Totuși, dacă aveți acces la un cluster de mașini Docker, vă
recomandăm să încercați să vă configurați o stivă de servicii care folosește volume cu
stocare externă.
:::

## NFS

Cea mai facilă metodă de a adăuga stocare externă în Docker este prin intermediul NFS
(Network File System). Pentru aceasta, este în primul rând nevoie de existența unui server
NFS, care poate fi rulat fie nativ (pe una din mașinile din cluster-ul Docker sau undeva în
exterior), fie prin intermediul unui container Docker. Există multiple soluții de rulare a
unui server NFS folosind Docker, însă noi vă recomandăm
[această soluție populară](https://hub.docker.com/r/itsthenetwork/nfs-server-alpine/)
bazată pe Alpine Linux.

Soluția de mai sus reprezintă un container care rulează în mod privilegiat, având acces la
resursele gazdei. Înainte să rulați containerul, trebuie să vă creați un director ce va fi
partajat în rețea prin intermediul NFS.

:::caution
Directorul partajat trebuie să nu fie de tipul OverlayFS.
:::

:::tip
Pentru a vedea sistemul de fișiere, se poate executa comanda <em>**df -Th**</em> (pe Linux).
:::

Dupa ce s-a creat directorul ce va reține datele partajate în rețea, se execută comanda de
mai jos pentru pornirea serverului de NFS și exportul directorului partajat:

```shell showLineNumbers
$ docker run -d --name nfs --privileged -v /cale/director/partajat:/nfsshare \
      -e SHARED_DIRECTORY=/nfsshare itsthenetwork/nfs-server-alpine:latest
```

În comanda de mai sus, directorul pe care dorim să-l partajăm se află la calea
<em>**/cale/director/partajat**</em>, fiind mapat pe serverul NFS la calea
<em>**/nfsshare**</em>. Odată ce serverul NFS a fost pornit, se pot crea volume peste
NFS în mai multe moduri. O variantă este crearea de volume prin intermediul API-ului din
linia de comandă. Astfel, dacă se dorește crearea unui volum numit <em>**mynfsvol**</em>,
care apoi poate fi atașat la alte containere sau servicii, se poate executa următoarea
comandă:

```shell showLineNumbers
$ docker volume create --driver local --opt type=nfs --opt o=nfsvers=3,addr=192.168.99.1,rw \
      --opt device=:/nfsshare mynfsvol
```

În comanda de mai sus, IP-ul serverului NFS este 192.168.99.1 și se folosește versiunea 3 de
NFS. Se poate inspecta volumul nou-creat pentru a se observa caracteristicile sale:

```shell showLineNumbers
$ docker volume inspect mynfsvol
[
    {
        "CreatedAt": "2021-03-31T17:26:11Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/mnt/sda1/var/lib/docker/volumes/mynfsvol/_data",
        "Name": "mynfsvol",
        "Options": {
            "device": ":/nfsshare",
            "o": "nfsvers=3,addr=192.168.99.1,rw",
            "type": "nfs"
        },
        "Scope": "local"
    }
]
```

Un astfel de volum extern poate fi dat ca parametru la pornirea unui container din linia
de comandă, astfel:

```shell showLineNumbers
$ docker run -v mynfsvol:/test -it alpine
```

În comanda de mai sus, se mapează volumul NFS la calea <em>**/test**</em> din interiorul
containerului. Acest lucru se poate face simultan pe mai multe containere de pe mașini
diferite, toate având aceeași mapare și deci acces la aceleași fișiere.

Putem folosi un volum creat în linia de comandă chiar într-un fișier de Docker Compose, fie
la rularea locală, fie la rularea folosind Docker Swarm. Pentru acest lucru, este nevoie de
câteva modificări în configurația volumelor din cadrul fișierelor YAML. Ca exemplu,
presupunem că avem un serviciu de bază de date care are nevoie de volume pentru scriptul de
configurație și pentru persistența datelor (pe modelul temelor de casă din laboratoarele
precedente):

```yaml showLineNumbers
services:
    db:
        image: postgres:12
        volumes:
            - db-data-nfs:/var/lib/postgresql/data
            - db-config-nfs:/docker-entrypoint-initdb.d

volumes:
    db-data-nfs:
        external: true
    db-config-nfs:
        external: true
```

În exemplul de mai sus, se presupune că volumele <em>**db-data-nfs**</em> și
<em>**db-config-nfs**</em> au fost create în prealabil folosind comenzile prezentate
anterior. Este totuși posibil să se creeze volumele direct în fișierul Docker Compose, așa
cum s-a prezentat și în laboratoarele precedente. În acest caz, volumele nu vor mai fi
declarate ca fiind externe, ci trebuie configurate pentru a funcționa peste NFS, astfel:

```yaml showLineNumbers
volumes:
    db-data-nfs:
        driver: local
        driver_opts:
           type: nfs
           o: "nfsvers=3,addr=192.168.99.1,nolock,soft,rw"
           device: :/database/data
    db-config-nfs:
        driver: local
        driver_opts:
           type: nfs
           o: "nfsvers=3,addr=192.168.99.1,nolock,soft,rw"
           device: :/database/config
```

În exemplul de mai sus, s-a folosit același IP pentru serverul NFS (192.168.99.1), iar
directoarele <em>**/database/data**</em> și <em>**/database/config**</em> trebuie să existe
în interiorul directorului partajat de gazda containerului de server NFS
(<em>**/cale/director/partajat**</em> din exemplul de pornire a serverului NFS).

O altă modalitate de a adăuga persistență folosind NFS ar fi montarea directoarelor de NFS
pe toate nodurile (sau pe toți managerii) din cluster, și apoi utilizarea de bind mounts
pentru a mapa căile de NFS montate local în interiorul serviciilor Docker.

## GlusterFS

[GlusterFS](https://www.gluster.org/) este un sistem de fișiere de rețea open-source,
utilizat pentru stocare în cloud sau streaming media. Pentru a putea folosi volume peste
GlusterFS, este necesară instalarea și configurarea sa pe fiecare nod din cluster. În
continuare, sunt prezentate comenzile necesare instalării pe un sistem bazat pe Ubuntu:

```shell showLineNumbers
$ apt-get install software-properties-common -y
```

```shell showLineNumbers
$ add-apt-repository ppa:gluster/glusterfs-3.12
```

```shell showLineNumbers
$ apt-get update
```

```shell showLineNumbers
$ apt install glusterfs-server -y
```

Odată instalat, GlusterFS se pornește cu următoarele comenzi:

```shell showLineNumbers
$ systemctl start glusterd
```

```shell showLineNumbers
$ systemctl enable glusterd
```

În continuare, nodul leader al clusterului (care va fi, cel mai probabil, nodul coordonator
GlusterFS) va trebui să execute următoarea comandă pentru fiecare din celălalte noduri din
cluster, pentru a conecta nodurile pentru sistemul de fișiere:

```shell showLineNumbers
$ gluster peer probe <HOSTNAME_NOD>
```

```shell showLineNumbers
$ docker image pull alpine
```

Conectarea nodurilor se poate verifica prin următoarea comandă dată pe manager:

```shell showLineNumbers
$ gluster pool list
```

Următorul pas presupune crearea unui volum partajat pe întreg clusterul, printr-o comandă
executată pe manager. În exemplul de mai jos, se presupune că avem un manager (cu
hostname-ul <em>**docker-manager**</em>) și doi workeri (<em>**docker-worker1**</em>
și <em>**docker-worker2**</em>). Toate trei nodurile vor avea un volum partajat (numit
<em>**mysharedvol**</em>) la calea <em>**/gluster/mysharedvol**</em>, deși nu este
obligatoriu să fie aceeași cale la toate nodurile.

```shell showLineNumbers
$ gluster volume create mysharedvol replica 3 docker-manager:/gluster/mysharedvol \
      docker-worker1:/gluster/mysharedvol docker-worker2:/gluster/mysharedvol force
```

În continuare, managerul trebuie să pornească volumul, astfel:

```shell showLineNumbers
$ gluster volume start mysharedvol
```

Ultimul pas se referă la montarea volumului, astfel încât să se poate accesa și după un
restart sau în alte situații. Comenzile de mai jos trebuie executate pe toate nodurile din
cluster:

```shell showLineNumbers
$ echo 'localhost:/mysharedvol /mnt glusterfs defaults,_netdev,backupvolfile-server=localhost 0 0' >> /etc/fstab
```

```shell showLineNumbers
$ mount.glusterfs localhost:/mysharedvol /mnt
```

```shell showLineNumbers
$ chown -R root:docker /mnt
```

În acest moment, în directorul <em>**/mnt**</em> se va găsi mapat, pe fiecare nod din
cluster, volumul partajat de GlusterFS. Similar cu NFS, acesta va putea fi folosit în
fișiere Docker Compose ca un bind mount pentru a asigura persistență distribuită în
aplicațiile noastre, astfel:

```yaml showLineNumbers
services:
    db:
        volumes:
            - /mnt/db-config:/docker-entrypoint-initdb.d/
            - /mnt/db-data:/var/lib/postgresql/data
```

## SSHFS

O altă variantă de a realiza persistență corectă într-un Docker Swarm este prin crearea de
volume folosind alt driver de volum decât cel implicit (care le creează local). Un astfel de
exemplu este driver-ul de SSHFS, numit [vieux/sshfs](https://hub.docker.com/r/vieux/sshfs).
SSHFS este un client de sistem de fișiere folosit pentru a monta și interacționa cu
directoare și fișiere aflate pe un server, prin intermediul unei conexiuni SSH.

Pentru a putea folos driver-ul de SSHFS, acesta trebuie instalat pe toate gazdele Docker:

```shell showLineNumbers
$ docker plugin install --grant-all-permissions vieux/sshfs
```

Odată ce driver-ul a fost instalat, putem crea un volum peste SSFHS astfel:

```shell showLineNumbers
$ docker volume create --driver vieux/sshfs -o sshcmd=remoteuser@remotenode:/home/remoteuser \
      -o password=remotepassword sshvolume
```

Astfel, se creează local un volum peste SSH, aflat pe serverul <em>**remotenode**</em>.
La server, conectarea se face cu numele de utilizator <em>**remoteuser**</em> și parola
<em>**remotepassword**</em> (dacă există chei partajate configurate între client și server,
nu mai este necesară parola). Volumul astfel creat poate fi folosit ca orice alt volum, iar
tot ce se scrie în el va fi persistat la server.

Volumul peste SSHFS se poate crea direct la rularea unui container, astfel:

```shell showLineNumbers
$ docker run -d --volume-driver vieux/sshfs \
    --mount src=sshvolume,target=/myvol,volume-opt=sshcmd=remoteuser@remotenode:/home/remoteuser,volume-opt=password=remotepassword \
    alpine
```

:::tip
Puteți testa SSHFS în
[acest sandbox](https://training.play-with-docker.com/docker-volume-sshfs/)
de Play with Docker.
:::