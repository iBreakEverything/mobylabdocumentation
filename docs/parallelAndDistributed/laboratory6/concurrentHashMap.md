---
title: Clasa ConcurrentHashMap
sidebar_position: 3
---

[ConcurrentHashMap](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/concurrent/ConcurrentHashMap.html) este o clasă care are aceleaşi funcţionalităţi ca un HashTable (nu HashMap, pentru că nu permite folosi *null* drept cheie sau valoare).

Dacă am folosi un HashMap clasic şi am adăuga un lock pentru accesul la structură, nu ar fi foarte eficient, pentru că doar un singur thread ar putea să o acceseze la un moment de timp, lucru care nu este necesar dacă, de exemplu, ele vor să modifice elemente diferite. Dacă, în schimb, am folosi câte un lock pentru fiecare element, ne-ar trebui destul de multe şi ne-ar îngreuna lucrul.

Într-un ConcurrentHashMap, operaţiile de tip **get** şi **put** blochează accesul la un anumit element (au mutexuri în implementarea lor), dar **NU** blochează accesul la întreaga structură de date. Cu alte cuvinte, două sau mai multe thread-uri o pot accesa în acelaşi timp, şi operaţiile de tip **get** se pot suprapune cu cele de tip update (**put**), dar implementarea se asigură că get-urile se execută la final, după ce s-au executat operațiile de update.

Metodele cele mai folosite sunt:

* **get(key)** - returnează valoarea asociată cu o cheie
* **contains(val)** - verifică dacă o valoare există în map
* **containsKey(key)** - verifică dacă o cheie există în map
* **put(key, val)** - adaugă o valoare la cheia specificată
* **putIfAbsent(key, val)** - adaugă o asociere doar dacă nu există deja o valoare pentru cheia respectivă; este întoarsă valoarea veche, dacă există, altfel e întors *null*
* **remove(key)** - şterge o asociere cheie-valoare
* **replace(key, val)** - modifică valoarea asociată cu o cheie.

Puteți observa mai jos un exemplu de utilizare.

```java showLineNumbers
ConcurrentHashMap<Integer, String> map = new ConcurrentHashMap<Integer, String>(); 
 
map.put(0, "Zero"); 
map.put(1, "One");
 
System.out.println(map.get(1)); // One
 
String val = map.putIfAbsent(1, "Unu"); // nu se modifică nimic, val va fi "One"
 
val = map.putIfAbsent(2, "Two"); // se adaugă maparea 2-"Two", val va fi null
```