---
title: Broadcast
sidebar_position: 3
---

## MPI_Bcast

MPI_Bcast reprezintă o funcție prin care un proces trimite un mesaj către toate procesele din comunicator (message broadcast), inclusiv lui însuși.

:::caution
În cadrul implementării MPI_Bcast sunt executate acțiunile de trimitere și de recepționare de mesaje, așadar nu trebuie să apelați MPI_Recv.
:::

Semnătura funcției este următoarea:

```c showLineNumbers
int MPI_Bcast(void* data, int count, MPI_Datatype datatype, int root, MPI_Comm communicator)
```
Unde:

* **data** (↓ + ↑) - reprezintă datele care sunt transmise către toate procesele. Acest parametru este de tip input pentru procesul cu identificatorul root și este de tip output pentru restul proceselor.
* **count** (↓) - dimensiunea datelor trimise
* **datatype** (↓)- tipul datelor trimise
* **root** (↓) - rangul / identificatorului procesului sursă, care trimite datele către toate procesele din comunicator, inclusiv lui însuși
* **tag** (↓) - identificator al mesajului
* **communicator** (↓) - comunicatorul în cadrul căruia se face trimiterea datelor către toate procesele din cadrul acestuia (de regulă **MPI_COMM_WORLD**)

O ilustrație care arată cum funcționează MPI_Bcast aveți mai jos:

<img alt="img" src="/img/parallel-and-distributed/mpiBcast.png" width="50%" style={{margin: "auto", display: "block"}} />