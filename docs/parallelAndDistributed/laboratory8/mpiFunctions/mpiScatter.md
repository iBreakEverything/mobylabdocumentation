---
title: Scatter
sidebar_position: 4
---

## MPI_Scatter

MPI_Scatter este o funcție prin care un proces împarte un array pe bucăți egale ca dimensiuni, unde fiecare bucată revine, în ordine, fiecărui proces, și le trimite tuturor proceselor din comunicator, inclusiv lui însuși.

Semnătura funcției este următoarea: 

```c showLineNumbers
int MPI_Scatter(void* send_data, int send_count, MPI_Datatype send_datatype, void* recv_data, int recv_count, MPI_Datatype recv_datatype, int root, MPI_Comm communicator)
```

Unde:

* **send_data** (↓) - reprezintă datele care sunt împărțite și trimise către procesele din comunicator
* **send_count** (↓) - reprezintă dimensiunea bucății care revine fiecărui proces (de regulă se pune ca fiind dimensiunea_totală / număr_de_procese).
* **send_datatype** (↓) - tipul datelor trimise către procese
* **recv_data** (↑) - reprezintă datele care sunt primite și stocate de către procese
* **recv_count** (↓) - dimensiunea datelor primite (de regulă dimensiunea_totală / număr_de_procese)
* **recv_datatype** (↓) - tipul datelor primite de către procese (de regulă este același cu send_datatype)
* **root** (↓) - identificatorul procesului care împarte datele și care le trimite către procesele din comunicator, inclusiv lui însuși
* **communicator** (↓) - comunicatorul din care fac parte procesele (de regulă **MPI_COMM_WORLD**)

O ilustrație a modului cum funcționează MPI_Scatter:

<img alt="img" src="/img/parallel-and-distributed/mpiScatter.png" width="50%" style={{margin: "auto", display: "block"}} />