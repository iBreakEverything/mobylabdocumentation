---
title: Algoritmul inel
sidebar_position: 4
---

Algoritmul inel este un algoritm de tip undă, care funcționează pe topologii sub formă de inel, unde fiecare nod proces are un vecin dedicat (Urm / next). În cadrul topologiilor inel, transmisia datelor se face folosind adresarea prin direcție, unde mai precis un nod proces trimite datele către vecinul său (Urm), astfel în cadrul topologiei formându-se un ciclu Hamiltonian.

În cadrul algoritmului, avem un nod proces inițiator, care trimite un jeton către vecinul său, care este pasat de fiecare proces către vecinul său de-a lungul ciclului, până când jetonul ajunge înapoi până la nodul proces inițiator.

Pseudocod:

```c showLineNumbers
chan token[1..n] (tok_type tok) //  canalul de comunicatie

// initiator
process P[i] {
    tok_type tok;
    send token[next](tok); // trimite token-ul la urmatorul nod
    receive token[i](tok); // primeste token-ul de la nodul precedent
    decizie;
}

// neinitiator
process P[k, k = 1..n, k != n] {
    tok_type tok;
    receive token[k](tok); // primeste token-ul de la nodul precedent
    send token[next](tok); // trimite token-ul la urmatorul nod
}
```

Pentru a vedea execuția pe pași al acestui algoritm, aveți această [prezentare](/files/parallel-and-distributed/ringAlgorithm.pdf) la dispoziție.