---
title: Barieră distribuită
sidebar_position: 3
---

În MPI există conceptul de barieră, care este similar ca funcționalitate cu cea din pthreads și din Java threads. Mai precis, bariera din MPI asigură faptul că niciun proces din cadrul comunicatorului nu poate trece mai departe de punctul în care este plasată bariera decât atunci când toate procesele din comunicator ajung în acel punct.

Semnătura funcției (varianta blocantă):

```c showLineNumbers
int MPI_Barrier(MPI_Comm comm)
```

Unde parametrul comm reprezintă comunicatorul MPI în care rulează procesele.

De asemenea, există o versiune non-blocantă a barierei în MPI:

```c showLineNumbers
int MPI_Ibarrier(MPI_Comm comm, MPI_Request *request)
```

Use case: Vreau să mă asigur că un proces a terminat de scris înainte ca altul să citească. Exemplu de folosire:

```c showLineNumbers
#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
 
int main(int argc, char **argv) {
 
    MPI_File out;
    int rank, numtasks;
    int ierr;
 
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
 
    remove("out.txt");
 
    ierr = MPI_File_open(MPI_COMM_WORLD, "out.txt", MPI_MODE_CREATE | MPI_MODE_RDWR, MPI_INFO_NULL, &out);
    if (ierr) {
        if (rank == 0) fprintf(stderr, "%s: Couldn't open output file %s\n", argv[0], argv[2]);
        MPI_Finalize();
        exit(1);
    }
 
 
    if (rank == 0) {
        char message_to_write[5] = "hello";
    	MPI_File_write_at(out, 0, message_to_write, 5, MPI_CHAR, MPI_STATUS_IGNORE);
    }
 
    MPI_Barrier(MPI_COMM_WORLD);
 
    if (rank == 1) {
    	char message_to_read[5];
    	MPI_File_read_at(out, 0, message_to_read, 5, MPI_CHAR, MPI_STATUS_IGNORE);
    	printf("\nMesajul citit este: ");
        for (int i = 0; i < 5; i++)
    	    printf("%c", message_to_read[i]);
        printf("\n");
    }
 
    MPI_File_close(&out);
 
    MPI_Finalize();
    return 0;
}
```

Pentru citirea și scrierea în fișier am folosit funcțiile MPI_File_read_at și MPI_File_write_at. Amândouă citesc din fișier de la un offset specificat.

```c showLineNumbers
int MPI_File_read_at(MPI_File fh, MPI_Offset offset, void *buf, int count, MPI_Datatype datatype, MPI_Status *status)
```

:::tip
Mai multe despre lucrul cu fișiere în MPI puteți găsi aici: [MPI I/O](/files/parallel-and-distributed/mpiIo.pdf).
:::