---
title: Broadcast
sidebar_position: 3
---

Operația de broadcast este o operație prin care un proces trimite o valoare tuturor proceselor din cadrul comunicatorului. Această operație este reprezentată, în MPI, de MPI_Bcast (laboratorul 8).

Aici aveți atașate slide-uri, care descriu în detaliu pașii de implementare ai operației broadcast: [slides](/files/parallel-and-distributed/broadcast.pdf)

Pseudocod:

```c showLineNumbers
for (pas = 1; pas < nr_procese; pas *= 2)
	if (rank < pas and rank + pas < nr_procese)
		trimite la procesul cu rank-ul [rank + pas]
    else if (rank >= pas and rank < pas * 2)
            primește de la procesul cu rank-ul [rank - pas]
```