---
title: Scan
sidebar_position: 2
---

Operația de scan este similară operației de reduce (acumularea elementelor unei colecții într-un singur rezultat). Diferența față de reduce este că ultimul proces din comunicator deține rezultatul final, practic scan reprezintă inversul lui reduce. În plus, fiecare proces are un rezultat parțial acumulat, în sensul că procesul 0 are valorea P0, procesul 1 are valoarea P0 + P1, procesul 2 are valoarea P0 + P1 + P2, etc.

Exemplu:

```c showLineNumbers
l = [1, 2, 3, 4, 5, 6]
op = +
rezultat = 1 + 2 + 3 + 4 + 5 + 6 = 21

Pași:
[1, 2, 3, 4, 5, 6]
[1, 3, 3, 4, 5, 6]
[1, 3, 6, 4, 5, 6]
[1, 3, 6, 10, 5, 6]
[1, 3, 6, 10, 15, 6]
[1, 3, 6, 10, 15, 21] -> rezultatul este 21
```

Aici aveți atașate slide-uri, care descriu în detaliu pașii de implementare ai operației scan: [slides](/files/parallel-and-distributed/scan.pdf)

Pseudocod:

```c showLineNumbers
for (pas = 1; pas < nr_procese; pas *= 2)
	if (rank + pas < nr_procese)
		trimite la procesul cu rank-ul [rank + pas]
        if (rank - pas >= 0)
                primește de la procesul cu rank-ul [rank - pas]
                adună 
```