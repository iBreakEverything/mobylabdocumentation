---
title: Fire de execuție
sidebar_position: 1
---

Un fir de execuție (sau **thread** în engleză) este definit ca un flux independent de instrucțiuni care pot fi planificate de către sistemul de operare. Din punct de vedere al unui programator, un fir de execuție poate fi descris cel mai bine ca o funcție care rulează independent de programul principal, iar un program paralel (cu mai multe fire de execuție, sau **multi-threaded**) poate fi privit ca toată mulțimea de astfel de funcții care pot fi planificate să ruleze simultan și/sau independent de către sistemul de operare.

:::caution
Există o distincție foarte importantă între conceptul de **proces** și cel de **thread**! Veți intra în detaliu la alte materii, dar, în acest moment, este foarte important de reținut faptul că un proces este o instanță de rulare a unui program (și deci **două procese distincte nu partajează spațiul de adrese**, care include stiva de program, variabile, date, etc.), pe când un thread este o unitate de lucru a unui proces (deci **mai multe thread-uri pot avea acces partajat la variabile și alte date**).
:::

Datorită faptului ca firele de execuție ale aceluiași proces partajează resurse, modificările făcute de către un thread asupra acelor resurse (cum ar fi, de exemplu, închiderea unui fișier) vor fi observate de toate thread-urile acelui proces. Mai mult, doi pointeri cu aceeași valoare referă aceleași date, iar scrierea și citirea în/din aceeași zonă de memorie este posibilă, dar necesită sincronizare explicită din partea programatorului (mai multe detalii despre ce înseamnă sincronizarea și cum se realizează veți afla în laboratorul 2).

În general, programele care pot beneficia de implementare multi-threaded au câteva trăsături comune:

- conțin componente computaționale care se pot executa în paralel
- au date pe care se poate opera în paralel
- se blochează ocazional așteptând după I/O
- trebuie să răspundă la evenimente asincrone
- anumite componente de execuție au o prioritate mai mare decât altele.