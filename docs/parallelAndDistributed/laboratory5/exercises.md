---
title: Exerciții
sidebar_position: 5
---

1. Pornind de la [scheletul de cod](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab05/src), în pachetul **oneProducerOneConsumer** implementați algoritmul Producer-Consumer pentru un buffer de dimensiune 1.
2. Modificați algoritmul Producer-Consumer astfel încât să accepte mai mulți producători și mai mulți consumatori. De asemenea, modificați buffer-ul astfel încât să fie de dimensiune > 1. Porniți de la scheletul din pachetul **multipleProducersMultipleConsumersNBuffer**.
3. Rezolvați problema din algoritmul filozofilor (pachetul **philosophersProblem**) și explicați-o.
4. Rezolvați problema Readers-Writers folosind prima soluție cu sincronizare condiționată din curs (pachetul **readersWriters.conditionedSynchronization**).
5. Rezolvați problema Readers-Writers, unde scriitori au prioritate (pachetul **readersWriters.writerPriority**).
6. Rezolvați problema bărbierului (pachetul **barber**).

:::caution
Exercițiile din cadrul acestui laborator trebuie rezolvate folosind mecanisme de sincronizare precum primitivele wait/notify/notifyAll sau semafoare. Nu se accepta soluții ce folosesc obiecte concurente.
:::