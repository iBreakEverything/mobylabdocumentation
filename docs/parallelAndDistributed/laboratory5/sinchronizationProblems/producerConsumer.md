---
title: Producător - Consumator
sidebar_position: 1
---

Problema se referă la două thread-uri: producător și consumator. Producătorul inserează date într-un buffer, iar consumatorul extrage date din acel buffer. Buffer-ul are o dimensiune prestabilită, astfel că:

* producătorul nu poate insera date dacă buffer-ul este plin
* consumatorul nu poate extrage date dacă buffer-ul este gol
* producătorul și consumatorul nu pot acționa simultan asupra buffer-ului

O implementare corectă a problemei presupune asigurarea faptului că nu vor exista situații de deadlock, adică situații în care cele două thread-uri așteaptă unul după celălalt, neexitând posibilitatea de a se debloca.

Această problemă se poate rezolva în mai multe moduri (rezolvările sunt mai sus, în cadrul textului laboratorului):

* folosind semafoare
* folosind variabile condiție

Pseudocod - variante cu semafoare:

```c showLineNumbers
T[] buffer = new T[k];
semaphore gol(k);
semaphore plin(0);
mutex mutex;

producer(int id) {
    T v;
    while (true) {
        v = produce();
		gol.acquire();

		mutex.lock();
		buf.add(v);
		mutex.unlock();
		
		plin.release();
    }
}

consumer(int id) {
    T v;
    while (true) {
        plin.acquire();

		mutex.lock();
		v = buf.poll();
		mutex.unlock();

		gol.release();
		consume(v);
    }
}
```

<a href="/files/parallel-and-distributed/producerConsumer.pdf" target="_blank">CheatSheet Producer-Consumer</a>