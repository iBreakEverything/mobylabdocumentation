---
title: Cuvântul cheie volatile
sidebar_position: 4
---

Cuvântul rezervat **volatile** asociat unei variabile specifică faptul că acea variabilă nu poate fi optimizată (plasată într-un registru sau exclusă din condiții pe baza unor invarianți de la compilare) și fiecare scriere sau citire asociată acesteia se va realiza lucrând cu memoria RAM. Acest lucru este util în prevenirea unor optimizări incompatibile în combinație cu modificarea valorii din alt thread, respectiv a citirii unei date neactualizate din registrul asociat core-ului pe care rulează un thread în momentul în care variabila a căpătat o valoare nouă pe alt thread.

```java showLineNumbers
public static volatile int counter = 0;
```

:::caution
Este important de menționat faptul că variabilele volatile cresc timpul de execuție al programului, deoarece fiecare modificare adusă lor trebuie propagată și celorlalte thread-uri, în timp ce variabilele normale pot fi cache-uite la un moment dat chiar până la nivelul regiștrilor procesorului (în cazul unui contor de buclă).
:::