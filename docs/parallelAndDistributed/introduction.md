---
title: Algoritmi Paraleli și Distribuiți
sidebar_position: 1
---

<img alt="img" src="/img/parallel-and-distributed/APD.png" width="400" style={{margin: "auto", display: "block"}} />

# Despre

Materia "Algoritmi Paraleli și Distribuiți" formează competențe necesare pentru rezolvarea problemelor prin soluții paralele sau distribuite. Se vor aborda concepte de bază și modele de programare paralelă și distribuită, metode de proiectare a soluțiilor paralele și distribuite, modalități de implementare a soluțiilor folosind limbaje de programare sau biblioteci specifice, metode de îmbunătățire a performanței soluțiilor folosind modele de complexitate, etc.

Studenții vor căpăta deprinderi legate de proiectarea și implementarea algoritmilor paraleli și distribuiți, dar și de analiza și alegerea soluțiilor potrivite pentru crearea de produse software folosind tehnologii de sisteme distribuite.

Materia urmărește, de asemenea, dobândirea unui fundament solid (teorie și aplicații practice) privind algoritmii paraleli și distribuiți. După absolvirea materiei, studenții vor putea să proiecteze soluții corecte în care sunt aplicați algoritmi paraleli și distribuiți, precum și să evalueze critic proprietățile de complexitate ale unor aplicații, înțelegând totodată alternativele existente pentru diverse sisteme proiectate.

# Echipa

## Curs

- Seria CA: Ciprian Dobre
- Seria CB: Elena Apostol
- Seria CC: Radu-Ioan Ciobanu
- Seria CD: Dorinel Filip

## Laborator

- Gabriel Guțu-Robu
- Valentin Bercaru
- Ioana-Valentina Marin
- Diana-Mihaela Megelea
- Sergiu-Cristian Toader
- Lucian Oprea
- Lucian-Florin Grigore
- Cătălin-Lucian Picior
- Cosmin-Viorel Lovin
- Daria Corpaci
- Dan-Andrei Borugă
- Vlad Stanciu
- Elena-Beatrice Anghel
- Robert-Mihai Adam
- Catalin-Alexandru Rîpanu
- Alexandru-Constantin Bala
- Andreea Borbei
- Alexandra-Ana-Maria Ion
- George-Alexandru Tudor
- Costin-Alexandru Deonise
- Taisia-Maria Coconu
- Dragoș Sofia

## Colaboratori

- Radu Marin
- Silviu Pantelimon
- Silvia-Elena Nistor
- George-Mircea Grosu
- Andrei Damian
- Corina Mihăilă
- Teia Vava
- Bogdan Oprea
- Florin Mihalache
- Lucian Iliescu
- Paul-Eduard Melinte
- Mihai Ungureanu
- Dorian Verna
- Theodor Oprea
- Andreea-Cristina Stan

# Orar

## Curs

* **CA**: Luni 14-16 (EC105), Joi 10-12 impar (EC105)
* **CB**: Marți 16 (EC004), Joi 16-18 par (PR001)
* **CC**: Miercuri 16-18 (PR001), Joi 16-18 impar (PR001)
* **CD**: Vineri 12-14 (EC101), Miercuri 16-18 impar (EC101)

## Laborator

<div style={{textAlign: "center"}}><iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSMg0ljMMh1nbd_T-YWx9R_-ipxl51CGEU6_O1z4HegQ2O77_-ftCGBW70P9UUXTDdlk1Zo2vqal6I5/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false" frameBorder="0" width="100%" height="500"></iframe></div>