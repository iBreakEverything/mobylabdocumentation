---
title: Comunicarea între componente și rutare
sidebar_position: 4
---

## Comunicare între componente

Comunicarea între componente este importantă pentru a construi aplicații complexe. În React, comunicarea între componente poate fi realizată prin transmiterea de proprietăți de la o componentă părinte la o componentă copil sau prin utilizarea unei funcții **callback** pentru transmiterea datelor dinspre copil spre părinte.

Componentele pot transmite date către componente copil prin intermediul proprietăților, iar acestea pot fi modificate în funcție de nevoile componente copil. Astfel, orice modificare a proprietăților din componenta părinte va fi reflectată automat în componenta copil.

Iată un exemplu de utilizare a acestui mod de comunicare:

```tsx showLineNumbers
import React from 'react';

type ChildProps = {
    data: { 
        lastName: string, 
        firstName: string
    }
}

function Parent() {
  const data = { lastName: 'Popescu', firstName: 'Ion' };

  return (
    <div>
      <h1>Welcome, {data.lastName} {data.firstName}!</h1>
      <Child data={data} />
    </div>
  );
}

function Child(props: ChildProps) {
  const { lastName, firstName } = props.data;

  return <div>
      <p>LastName: {lastName}</p>
      <p>FirstName: {firstName}</p>
    </div>
}
```

:::note
În acest exemplu, componenta părinte (Parent) definește un obiect data cu informații despre utilizatorul curent. Acest obiect este transmis către componenta copil (Child) ca proprietatea `data`. Componenta copil primește această proprietate și afișează informațiile în elemente `p`.
:::

Pentru a comunica dintr-o componentă copil către o componentă părinte în React.js, se poate folosi o funcție callback. Această metodă constă în definirea unei funcții în componenta părinte, pe care o transmitem apoi către componenta copil ca proprietate. Componenta copil poate apela funcția atunci când are nevoie să transmită informații înapoi către componenta părinte.

Iată un exemplu de utilizare a acestei metode:

```tsx showLineNumbers
import React, { useState } from 'react';

function Parent() {
  const [message, setMessage] = useState('');

  // Definim o funcție care primește un mesaj
  // și îl setează ca stare în componenta părinte
  const handleMessageExchanged = (newMessage) => {
    setMessage(newMessage);
  }

  return (
    <div>
      <p>Mesajul primit: {mesaj}</p>
      <Child onMessageExchanged={handleMessageExchanged} />
    </div>
  );
}

function Child(props) {
  const [message, setMessage] = useState('');

  // Funcție care apelează funcția de la părinte pentru a trimite mesajul
  // din copil înapoi către părinte
  const handleClick = () => {
    props.onMessageExchanged(message);
  }

  return (
    <div>
      <input type="text" value={message} onChange={(event) => setMesaj(event.target.value)} />
      <button onClick={handleClick}>Send message</button>
    </div>
  );
}
```

:::note
În acest exemplu, componenta părinte definește o funcție `handleMessageExchanged` care primește un mesaj și îl setează ca stare în componenta părinte. Această funcție este transmisă apoi către componenta copil ca proprietatea `onMessageExchanged`. Componenta copil definește o funcție `handleClick` care apelează funcția de la părinte cu mesajul introdus în câmpul de text. Când utilizatorul apasă butonul "Send message", mesajul este trimis înapoi către componenta părinte și afișat în elementul `p` din aceasta.

Această metodă este utilă în situații în care componentele copil trebuie să transmită date către componentele părinte.
:::

## Rutare in React

### Ce este rutarea și cum funcționează în React?

Rutarea este procesul de gestionare a navigării între diferite pagini sau secțiuni ale unei aplicații web. În React, rutarea este gestionat prin intermediul unei biblioteci numite React Router. React Router oferă un mecanism simplu de definire a rutelor în aplicație și de afișare a componentelor corespunzătoare acestor rute.

În React, rutarea este definită ca o asociere între un URL și o componentă.

Pentru a gestiona rutele în React, trebuie mai întâi instalată biblioteca React Router folosind un manager de pachete, cum ar fi npm:

```sh
npm install react-router-dom
```

### Utilizarea React Router

După ce a fost instalat React Router, se pot defini rutele în aplicație.

```tsx showLineNumbers
import { BrowserRouter, Route, Route, Link } from 'react-router-dom';
import HomePage from './HomePage';
import AboutUsPage from './PaginaDespre';
import ContactPage from './ContactPage';

function App() {
  return (
    <BrowserRouter>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home page</Link>
            </li>
            <li>
              <Link to="/despre">About us</Link>
            </li>
            <li>
              <Link to="/contact">Contact</Link>
            </li>
          </ul>
        </nav>
        <Routes>
            <Route path="/" exact element={<HomePage />} />
            <Route path="/about" element={<AboutUsPage />} />
            <Route path="/contact" element={<ContactPage />} />
        <Routes>
      </div>
    </BrowserRouter>
  );
}
```

:::note
În acest exemplu, am definit trei rute diferite, fiecare asociată cu o componentă. Prima rută, definită cu exact path="/", este asociată cu componenta `HomePage` și va fi afișată atunci când utilizatorul navighează la URL-ul rădăcină al aplicației. A doua rută, definită cu path="/about", este asociată cu componenta `AboutUsPage` și va fi afișată atunci când utilizatorul navighează la URL-ul "/about". A treia rută, definită cu path="/contact", este asociată cu componenta `ContactPage` și va fi afișată atunci când utilizatorul navighează la URL-ul "/contact".
:::

O altă caracteristică puternică a React Router este capacitatea de a utiliza parametri în rute. Aceasta poate fi utilă, de exemplu, pentru a afișa detaliile unui anumit element dintr-o listă.

```tsx showLineNumbers
import { BrowserRouter, Route, Route, Link } from 'react-router-dom';
import HomePage from './HomePage';
import ProductListPage from './ProductListPage';
import ProductDetailsPage from './ProductDetailsPage';

function App() {
  return (
    <BrowserRouter>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home page</Link>
            </li>
            <li>
              <Link to="/products">Products</Link>
            </li>
          </ul>
        </nav>
        <Routes>
            <Route path="/" exact element={<HomePage />} />
            <Route path="/products" exact element={<ProductListPage />} />
            <Route path="/products/:id" element={<ProductDetailsPage />} />
        <Routes>
      </div>
    </BrowserRouter>
  );
}
```

:::note
În exemplul de mai sus, ruta `/products/:id` include un parametru id care poate fi utilizat pentru a afișa detaliile produsului cu identificatorul specificat. Acest parametru poate fi accesat în componenta `ProductDetailsPage` folosind hook-ul `useMatch` din React Router.

```typescript showLineNumbers
const match = useMatch("/products/:id");
const productId = match?.params.id;
```
:::