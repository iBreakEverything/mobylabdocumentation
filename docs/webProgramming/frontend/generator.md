---
title: OpenAPI Generator
sidebar_position: 8
---

## Ce este OpenAPI?

**OpenAPI Specifications** sau **OpenAPI**, anterior cunoscut sub numele de **Swagger**, este un set de specificații și instrumente open source care permit dezvoltatorilor să definească, să documenteze și să utilizeze servicii web RESTful.

:::note
OpenAPI definește o formă standardizată de descriere a interfeței API-urilor, inclusiv informații despre rutele API, parametri, tipurile de date și răspunsurile pe care le returnează serviciul.
:::

Prin utilizarea OpenAPI, dezvoltatorii pot crea documentații clare și ușor de înțeles pentru serviciile lor web, ceea ce face mai ușor pentru utilizatorii acestor servicii să le utilizeze și să le integreze în propriile aplicații. De asemenea, OpenAPI permite dezvoltatorilor să genereze automat codul clientului pentru interacțiunea cu un API, ceea ce poate reduce timpul și efortul necesar pentru a crea clientul API-ului.

OpenAPI este utilizat de multe companii mari, inclusiv Amazon, Google și Microsoft, și este susținut de comunitatea open source.

## Ce este OpenAPI Generator?

OpenAPI Generator este un instrument open source care permite generarea automată a codului clientului și al serverului pentru interacțiunea cu un API web, pe baza unei specificații OpenAPI. Acesta poate fi utilizat cu o varietate de limbaje de programare și platforme, inclusiv Java, JavaScript, Python, Ruby, C # și multe altele.

OpenAPI Generator poate fi folosit pentru a genera codul clientului API, care poate fi inclus în aplicația dvs. React.js. Acest lucru vă permite să interacționați cu API-ul utilizând metodele și tipurile de date definite în specificația OpenAPI, fără a fi necesară scrierea manuală a codului clientului.

În plus, OpenAPI Generator poate fi utilizat și pentru a genera codul serverului API, care poate fi folosit pentru a crea propriul dvs. API, bazat pe specificația OpenAPI. Acest lucru poate fi util dacă doriți să oferiți un API personalizat pentru propriile aplicații sau pentru a permite altor dezvoltatori să utilizeze serviciile dvs. web prin intermediul unui API.

OpenAPI Generator este disponibil ca o bibliotecă Java și poate fi instalat și utilizat ca o unealtă de linie de comandă. De asemenea, există și integrări disponibile pentru diferite platforme de dezvoltare, cum ar fi Maven, Gradle, Node.js și altele.

:::warning
Pentru a putea utiliza OpenAPI Generator trebuie sa aveți Java instalat pe calculator.
:::

## Utilizare OpenAPI Generator

Pentru a utiliza OpenAPI Generator într-un proiect React, urmați acești pași:

1. Instalați OpenAPI Generator utilizând un manager de pachete precum npm sau yarn. De exemplu, pentru a instala OpenAPI Generator folosind npm, rulați următoarea comandă:

```sh
npm install @openapitools/openapi-generator-cli -g
```

2. Generați codul clientului din specificația OpenAPI folosind OpenAPI Generator. De exemplu, pentru a genera codul clientului dintr-un fișier swagger.yaml și a-l salva într-un director numit client, rulați următoarea comandă:

```sh
openapi-generator generate -i swagger.yaml -g javascript -o client
```

Alternativ, pentru scheletul de laborator, porniți BE-ul si folosiți comanda:

```sh
openapi-generator-cli generate -i http://localhost:5000/swagger/v1/swagger.json -g typescript-fetch -o ./src/infrastructure/apis/client --additional-properties=supportsES6=true
```

3. În interiorul componentelor React, importați funcțiile generate și utilizați-le în mod corespunzător.

Ca exemplu de utilizare, dacă generatorul v-a generat din specificațiile OpenApi un API de preluat utilizatorii **UserApi**, atunci puteși folosi în felul următor API-ul generat:

```typescript
import { Configuration, UserApi, ApiUserGetPageGetRequest } from "./apis/client";

const getAuthenticationConfiguration = (token: string | null) => new Configuration(!isNull(token) ? { apiKey: `Bearer ${token}` } : {});
const getToken = () => localStorage.getItem("token") ?? null;
export const getUsers = (page: ApiUserGetPageGetRequest) => new UserApi(getAuthenticationConfiguration(getToken())).apiUserGetPageGet(page);
```