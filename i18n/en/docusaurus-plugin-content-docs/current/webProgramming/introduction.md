---
title: Programare Web
sidebar_position: 1
---

<img alt="img" src="/img/web-programming/webProgramming.webp" width="800" style={{margin: "auto", display: "block"}} />

## Despre

Materia de programare web se concentrează pe înțelegerea și dezvoltarea competențelor necesare pentru crearea și gestionarea aplicațiilor și site-urilor web. Această disciplină combină cunoștințe de programare cu concepte specifice mediului online pentru a oferi studenților instrumentele și abilitățile necesare pentru a crea aplicații web eficiente și interactive.

În cadrul acestei discipline, vom utiliza, atât în laborator, cât și în elaborarea proiectului, următoarele tehnologii:

* Docker pentru mediul de lucru
* PostgreSQL ca bază de date
* .NET 6 pentru dezvoltarea de server
* React.js pentru implementarea interfeței de utilizator

## Scopul laboratorului

Laboratorul de programare web își propune să ofere studenților cunoștințe fundamentale în acest domeniu și a fost conceput pentru a le furniza concepte esențiale în dezvoltarea aplicațiilor web. Prima jumătate a laboratorului va fi dedicată dezvoltării backend-ului în cadrul platformei .NET iar a doua jumătate se va concentra asupra dezvoltării frontend-ului.

## Curs

* [Ciprian Dobre](https://teams.microsoft.com/l/chat/0/0?users=ciprian.dobre@upb.ro)

## Echipa

* [Bogdan-Gabriel Georgescu](https://teams.microsoft.com/l/chat/0/0?users=bgeorgescu@upb.ro)
* [Silviu-George Pantelimon](https://teams.microsoft.com/l/chat/0/0?users=silviu.pantelimon@upb.ro)

## Cerințe proiect

Pentru realizarea proiectului trebuie să vă alegeți o temă de proiect pentru o aplicație web. La laborator se va face .NET (backend) și React (frontend) dar se pot folosi și alte tehnologii, mai precis Java Spring (backend) și Angular (frontend) pentru cine e familiarizat cu aceste tehnologii. Nu se vor accepta proiecte scrise cu alte tehnologii cum ar fi NodeJs (backend), Django, PHP sau Android.

:::tip
Deși aveți posibilitatea de a opta pentru Java Spring și Angular, recomandăm lucrați cu tehnologiile de la laborator și să plecați de la aplicația [demo](https://gitlab.com/mobylabwebprogramming) a noastră. Veți învăța tehnologii noi și în același timp veți putea implementa mai ușor și mai repede aplicația pentru proiect pentru că aveți deja multe componente pe care le puteți folosi.
:::

:::tip
Dacă alegeți să folosiți Spring, vedeți că aveți posibilitatea să îl folosiți și cu Kotlin în loc de Java. Kotlin este un limbaj mai flexibil decât Java și este interoperabil cu acesta, toate pachetele de Maven sau Gradle pot fi folosite cu Kotlin. 
:::

:::danger
Dacă lucrați în Spring, vă rugăm să nu folosiți EntityManager sau alte primitive expuse de ORM, folosiți interfețele repository de JPA și adnotările corespunzătoare pentru a interacționa cu ORM-ul și mai ales folosiți JPQL pentru a face cereri SQL complexe, nu să construiți de mână cererile SQL. De asemenea, pentru baza de date folosiți pentru migrații Liquibase sau Flyway dacă lucrați cu Spring, iar pentru Swagger folosiți pachetul de OpenAPI ca să-l aveți integrat în backend.
:::

### Backend

* **Repository Pattern/ORM**
  * Minim 5 entități (în afara de tabele de merge pentru relații many-to-many) – **3 puncte**
  * Relații entități (one-to-one, one-to-many, many-to-many) – **3 puncte**
  * Configurare relații utilizând FluentAPI/Atribute – **3 puncte**
* **Basic JWT Auth**
  * Adăugare claims în token (roluri, alte informații în afara de cele din scheletul de cod) – **3 puncte**
  * Permisiuni bazate pe roluri cu logica de verificat drepturile în backend – **3 puncte**
  * Autorizare controllere – **3 puncte**
* **CRUD Controllers**
  * Controllere Basic (Get cu ID, Get cu listă, Post, Put, Delete + altele) – **4 puncte**
  * Error Handling (error codes) – **3 puncte**
* **DTOs**
  * Utilizarea de DTO-uri pentru interacțiunea cu controllerele (request/response) – **5 puncte**

### Frontend
* **Register**
  * Formular register - nume, alte informații relevante, username/email + parolă/validare parolă (trebuie să funcționeze apoi login-ul) – **4 puncte**
* **Routing**
  * NavBar - minim 3 pagini (în afara de cele din scheletul de cod) – **4 puncte**
* **Table views**
  * Două pagini cu tabele sau liste paginate, cu minim 4 coloane/4 câmpuri cu informații afișate fiecare – **4 puncte**
  * Căutare – **3 puncte**
  * Paginare – **2 puncte**
* **Add/Edit/Delete date (pentru fiecare tabel/listă)**
  * Adăugare intrare – **2 puncte**
  * Editare intrare – **2 puncte**
  * Ștergere intrare – **2 puncte**
  * Modală confirmare – **2 puncte**
* **Formular feedback**
  * Un formular de feedback de la utilizatori (legat cu backend-ul) cu cel puțin un select, un buton radio, un checkbox și o căsuță de text - **5 puncte**
* **Bonus**
  * Prezentare proiect complet până în săptămâna 9 – **5 puncte** 
  * Funcționalități adiționale - **5 puncte**
:::note
Acordarea bonusului este la latitudinea asistentului. Bonusul se acorda pentru funcționalități ce depăsesc cerintele proiectului. Ex: Containerizare, Mockup-uri Figma/XD, Hosting, CI/CD, Notificări Push, Unit Testing etc. De asemenea, bonusul pentru prezentarea anticipată scalează cu punctajul obținut, de exemplu, dacă punctajul obținut pentru proiect este de 30/60, bonusul acordat este de 2.5 puncte.
:::