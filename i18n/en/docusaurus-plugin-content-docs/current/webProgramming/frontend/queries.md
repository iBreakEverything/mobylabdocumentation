---
title: ReactQuery
sidebar_position: 7
---

## Ce este ReactQuery?

ReactQuery este o bibliotecă pentru React care oferă o modalitate simplă de a gestiona starea și datele asincrone dintr-o aplicație React. Această librărie folosește un concept numit "queries" pentru a face apeluri la server și pentru a gestiona datele returnate. ReactQuery utilizează cache-ul local pentru a reduce numărul de cereri la server și pentru a oferi o experiență mai rapidă utilizatorilor.

React Query oferă, de asemenea, o serie de caracteristici utile, cum ar fi stale-while-revalidate (SWR), care permite afișarea imediată a datelor din cache, chiar și înainte de a fi actualizate cu datele din server, și focus pe performanță, care permite amânarea încărcării datelor până când componenta respectivă este încărcată.

:::note
În esență, React Query face mai ușor pentru dezvoltatori să gestioneze datele și starea în aplicații React, permițându-le să se concentreze mai mult pe construirea interfeței și mai puțin pe gestionarea datelor.
:::

## Uilizare React Query

1. Pentru a începe să utilizați ReactQuery, trebuie să îl instalați mai întâi. Puteți face acest lucru utilizând comanda de instalare npm:

```sh
npm install @tanstack/react-query
```

2. După ce ați instalat React Query, trebuie să îl importați și să îl configurați în aplicația voastră. React.js. În mod tipic, acest lucru se face în fișierul App.ts. Iată un exemplu simplu:

```tsx showLineNumbers
import React from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';

const queryClient = new QueryClient();

export const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <div className="App">
        {/* Restul componentelor tale */}
      </div>
    </QueryClientProvider>
  );
}
```

3. După ce ați configurat React Query, puteți începe să utilizați funcțiile hook **useQuery** și **useMutation** pentru a obține și a actualiza datele. Iată un exemplu de utilizare a useQuery:

```tsx showLineNumbers
import React from 'react';
import { useQuery } from "@tanstack/react-query";

type TaskData = { id: number, title: string }

export const TaskList = () => {
  const { data, isLoading, error } = useQuery({ // prin aceasta funcție hook avem deja abstractizată starea cererii fără să o definim manual
    queryKey: ["todos"], // lista de chei este folosită pentru a indexa rezultatul într-un cache și a-l invalida după expirare sau în urma unor acțiuni
    queryFn: async () => {
      const response = await fetch('https://jsonplaceholder.typicode.com/todos');
      return await response.json() as TaskData[]; // extragem ca înainte raspunsul 
    }
  });

  // în funcție de starea curenta afișăm corespunzator componenta
  if (error) {
    return <p>{error.message}</p>;
  }

  if (isLoading) {
    return <p>Loading...</p>;
  }

  return <div>
    <h1>Task list:</h1>
    <ul>
      {data?.map(task => <li key={task.id}>{task.title}</li>)} 
    </ul>
  </div>
}
```

:::note
În acest exemplu, am utilizat **useQuery** pentru a obține o listă de to-do-uri de la un API extern. useQuery primește un identificator pentru query ('todos' în acest caz) și o funcție de interogare care va fi apelată pentru a obține datele. Dacă query-ul este încărcat (isLoading), va fi afișat un mesaj "Loading...", dacă apare o eroare (error), va fi afișat un mesaj de eroare, altfel vor fi afișate datele. Observași și cum se simplifică codul față de varianta anterior prezentată folosind biblioteca ReactQuery.
:::

Pentru a întelege mai bine cum puteți folosi ReactQuery în proiectele voastre sugerăm să consultați [documentația oficială](https://tanstack.com/query/latest/docs/framework/react/overview).
