---
title: Automatic Memory Management
sidebar_position: 4
---

# About Automatic Memory Management

In general, there are two methods through which automatic memory management can be achieved in programming languages: **garbage collection (GC)** and **smart pointers**.

Smart pointers are special data structures that keep track of the reference count of a dynamically allocated object. In languages like C++, these structures typically use the **RAII (Resource Acquisition Is Initialization)** principle to manage dynamically allocated references. Smart pointers can be shared, counting references and shareable by multiple owners, or unique, where only one owner can hold a reference at a given time.

An alternative to smart pointers is garbage collection. At runtime, a program routine called the **garbage collector** tracks references, and as long as references are accessible from a reference on the stack, the memory for those references will not be recycled. Otherwise, objects will be destroyed, and memory deallocated. Languages with garbage collection are quite popular, such as C#, Java, and Go.

Both solutions have advantages and disadvantages:

|             | Advantages                             | Disadvantages                                     |
|:------------|:---------------------------------------:|:------------------------------------------------:|
| Smart pointers | Deterministic program runtimes     | Can be misused depending on specialization      |
|               | Better runtime performance in terms of time/memory consumed | Learning their usage can be more complex depending on use case |
|               | Easier to implement than GC         | Problems can still arise if used alongside raw pointers |
| Garbage collection | Programmers don't need to explicitly manage GC presence | Program runtimes are non-deterministic |
|                  | Easier to prevent memory leaks     | Making memory leaks is harder but not impossible, especially with forgotten threads |
|                  |                                  | More challenging to implement compared to smart pointers |
