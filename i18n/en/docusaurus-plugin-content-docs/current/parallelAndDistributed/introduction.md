---
title: Parallel and Distributed Algorithms
sidebar_position: 1
---

<img alt="img" src="/img/parallel-and-distributed/APD.png" width="400" style={{margin: "auto", display: "block"}} />

# About

The "Parallel and Distributed Algorithms" course provides the necessary skills for solving problems through parallel or distributed solutions. It covers fundamental concepts and models of parallel and distributed programming, methods for designing parallel and distributed solutions, ways to implement solutions using specific programming languages or libraries, and methods for improving performance using complexity models, etc.

Students will acquire skills related to the design and implementation of parallel and distributed algorithms, as well as the analysis and selection of appropriate solutions for creating software products using distributed systems technologies.

The course also aims to establish a solid foundation (both in theory and practical applications) regarding parallel and distributed algorithms. Upon completing the course, students will be able to design correct solutions that incorporate parallel and distributed algorithms and critically evaluate the complexity properties of various applications. Additionally, they will understand the available alternatives for different designed systems.

# Team

## Course

- CA series: Ciprian Dobre
- CB series: Elena Apostol
- CC series: Radu-Ioan Ciobanu
- CD series: Dorinel Filip

## Laboratory

- Gabriel Guțu-Robu
- Valentin Bercaru
- Ioana-Valentina Marin
- Diana-Mihaela Megelea
- Sergiu-Cristian Toader
- Lucian Oprea
- Lucian-Florin Grigore
- Cătălin-Lucian Picior
- Cosmin-Viorel Lovin
- Daria Corpaci
- Dan-Andrei Borugă
- Vlad Stanciu
- Elena-Beatrice Anghel
- Robert-Mihai Adam
- Catalin-Alexandru Rîpanu
- Alexandru-Constantin Bala
- Andreea Borbei
- Alexandra-Ana-Maria Ion
- George-Alexandru Tudor
- Costin-Alexandru Deonise
- Taisia-Maria Coconu
- Dragoș Sofia

## Collaborators

- Radu Marin
- Silviu Pantelimon
- Silvia-Elena Nistor
- George-Mircea Grosu
- Andrei Damian
- Corina Mihăilă
- Teia Vava
- Bogdan Oprea
- Florin Mihalache
- Lucian Iliescu
- Paul-Eduard Melinte
- Mihai Ungureanu
- Dorian Verna
- Theodor Oprea
- Andreea-Cristina Stan


# Schedule

## Course

* **CA**: Monday 14-16 (EC105), Thursday 10-12 odd (EC105)
* **CB**: Tuesday 16 (EC004), Thursday 16-18 even (PR001)
* **CC**: Wednesday 16-18 (PR001), Thursday 16-18 odd (PR001)
* **CD**: Friday 12-14 (EC101), Wednesday 16-18 odd (EC101)

## Lab

<div style={{textAlign: "center"}}><iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSMg0ljMMh1nbd_T-YWx9R_-ipxl51CGEU6_O1z4HegQ2O77_-ftCGBW70P9UUXTDdlk1Zo2vqal6I5/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false" frameBorder="0" width="100%" height="500"></iframe></div>