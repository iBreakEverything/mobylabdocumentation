---
title: Semaphores (continued)
sidebar_position: 1
---

In the previous lab, we saw how a semaphore is a generalization of a mutex. A semaphore initialized with 1 can be used as a lock because only one thread can access the critical section at a time. However, a semaphore can have much more complex uses since it can take various values, both negative and positive.

When a semaphore is initialized with a positive value *x*, you can think of it as allowing *x* threads to enter the critical section. As a thread calls **acquire()**, *x* is decremented. When it reaches 0, other potential threads that want to access the critical region will have to wait until the semaphore's value increases to a positive value (i.e., until a thread exits the critical region).

Conversely, when a semaphore is initialized with a negative value like -1, it expects at least two threads to first call **release()** (to increase the semaphore's value from -1 to 1) before a critical region can be accessed (i.e., another thread can call **acquire()**). You can imagine a third thread waiting "at the semaphore" for the other two threads to signal it by calling **release()** when they have "finished their job." You can see an example in the pseudocode below.

<table>
    <thead>
        <tr>
            <td colSpan="3">
                Semaphore sem = new Semaphore(-1);
            </td>
        </tr>
        <tr>
            <th>T0</th>
            <th>T1</th>
            <th>T2</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                // wait at semaphore
                <br />
                // after the 2 points
                <br />
                sem.acquire();
                <br />
                <br />
                <br />
                <br />
                <br />
                // sem = 1, so it can pass
                <br />
                System.out.println("I passed the semaphore!");
            </td>
            <td>
                <br />
                do_work1();
                <br />
                sem.release();
                <br />
                // sem = -1 + 1 = 0
                <br />
                <br />
                <br />
                <br />
            </td>
            <td>
                <br />
                <br />
                <br />
                <br />
                do_work2();
                <br />
                sem.release();
                <br />
                // sem = 0 + 1 = 1
                <br />
            </td>
        </tr>
    </tbody>
</table>