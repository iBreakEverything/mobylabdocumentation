---
title: Send
sidebar_position: 1
---

## MPI_Send

MPI_Send is the function through which one process sends data to another process. The function signature is as follows:

```c showLineNumbers
int MPI_Send(void* data, int count, MPI_Datatype datatype, int destination, int tag, MPI_Comm communicator)
```

In the context of:

* **data** (↓) - represents the data sent from the source process to the destination process
* **count** (↓) - the size of the transmitted data
* **datatype** (↓) - the type of data being transmitted
* **destination** (↓) - the rank or identifier of the destination process to which the data is sent
* **tag** (↓) - a message identifier
* **communicator** (↓) - the communicator within which data is sent between the two processes

MPI_Send is a blocking function. More specifically, the program will block until the buffer provided as the first parameter can be reused, even if the action of receiving the message sent by the current process (MPI_Recv) is not executed. If the situation arises where process P1 sends data (MPI_Send) to process P2, and P2 does not have enough space in its receive buffer (the buffer does not have enough free space or is full), then P1 will become blocked.