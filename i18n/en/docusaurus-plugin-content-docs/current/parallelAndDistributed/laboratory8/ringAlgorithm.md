---
title: Ring Algorithm
sidebar_position: 4
---

The ring algorithm is a wave-like algorithm that operates on ring-shaped topologies, where each process node has a dedicated neighbor (Next). In ring topologies, data transmission is done using directional addressing, where precisely, a process node sends data to its neighbor (Next), forming a Hamiltonian cycle within the topology.

In the algorithm, we have an initiator process node that sends a token to its neighbor, which is passed by each process to its neighbor along the cycle until the token returns to the initiator process node.

Pseudocode:

```c showLineNumbers
chan token[1..n] (tok_type tok) //  canalul de comunicatie

// initiator
process P[i] {
    tok_type tok;
    send token[next](tok); // trimite token-ul la urmatorul nod
    receive token[i](tok); // primeste token-ul de la nodul precedent
    decizie;
}

// neinitiator
process P[k, k = 1..n, k != n] {
    tok_type tok;
    receive token[k](tok); // primeste token-ul de la nodul precedent
    send token[next](tok); // trimite token-ul la urmatorul nod
}
```

To view the step-by-step execution of this algorithm, you have access to this [presentation](/files/parallel-and-distributed/ringAlgorithm.pdf).